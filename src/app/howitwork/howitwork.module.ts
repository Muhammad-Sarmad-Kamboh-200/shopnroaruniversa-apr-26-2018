import { NgModule } from '@angular/core';
import {CommonModule } from '@angular/common';
import { RecaptchaModule } from 'ng-recaptcha';
import {Loginrouting,appRoutingProviders} from './howitwork.routes';
import {Ng2PaginationModule} from 'ng2-pagination';
import { FormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';
import {PreloaderModule} from '../components/preloader.module';
import { HowitworkComponent } from '../howitwork/howitwork.component';
import {PartnersComponent } from '../partners/partners.component'
import {PartnerModule} from '../partners/partners.module'
@NgModule({
  declarations: [
    HowitworkComponent,

  ],
  imports: [
    CommonModule,
    PartnerModule,
    FormsModule,
    PreloaderModule,
    RecaptchaModule.forRoot(),
    HttpModule,
    Ng2PaginationModule,
    Loginrouting
  ],

})
export class WorkModule { }
