import { NgModule } from '@angular/core';
import {CommonModule } from '@angular/common'
import {Forgotpasswordrouting,appRoutingProviders} from './forgotpassword.routes';
import {Ng2PaginationModule} from 'ng2-pagination';
import { FormsModule } from '@angular/forms';

import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import {ForgotpasswordComponent} from "../forgotpassword/forgotpassword.component"
import {PreloaderModule} from '../components/preloader.module'
import { RecaptchaModule } from 'ng-recaptcha';

@NgModule({
  declarations: [
    ForgotpasswordComponent,
    // PreloaderFull,
    // PreloaderSmall,

    //

  ],
  imports: [
    CommonModule,
    FormsModule,
    PreloaderModule,
    RecaptchaModule.forRoot(),
    HttpModule,
    Ng2PaginationModule,
    Forgotpasswordrouting
  ],

})
export class ForgotModule { }
