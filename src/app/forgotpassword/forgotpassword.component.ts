import { Component, OnInit } from '@angular/core';
import {Md5} from 'ts-md5/dist/md5';
import {AlertService, UserService} from '../_services/index';
import { HttpService } from '../http.service';


@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css'],
  providers: [UserService,AlertService,HttpService]
})
export class ForgotpasswordComponent implements OnInit {


  model: any = {};
  mailSend:any;
  myLink="http://www.shopnroar.com/reset/"
  recaptcha :any;
  status:any
  Userloading = false;
  loading = false;

  emailloading = false;
  UserError;
  emailError;

  constructor(private userService: UserService,
              private alertService: AlertService,private httpService: HttpService) { }


  resolved(captchaResponse: string) {
    if (captchaResponse) {
      this.recaptcha = true;
    }
  }
  checkmail() {
    this.emailError=false;
    this.emailloading = true
    this.userService.check_email_unique(this.model.username)
      .subscribe(
        (response) => {
          // alert(this.model.email)

          this.emailloading = false;
          console.log(response)
          if (response.exists === 'Yes') {
            this.emailError = true;
          }
          if (response.exists !== 'Yes') {
            this.emailError = false;
          }

        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });

  }

  Recover() {

    if ( this.recaptcha === true )
    {
      this.userService.check_email_unique(this.model.username)
        .subscribe(
          (response) => {
            // alert(this.model.email)

            this.emailloading = false;
            // console.log(response)
            if (response.exists === 'Yes') {
              this.emailError = true;
              let hass =  new Date();

              let hash = Md5.hashStr(this.model.username+""+hass);
              this.myLink=this.myLink+""+hash
              this.httpService.Add_link(this.model.username,this.myLink).subscribe(
                (response) => {
                  this.mailSend=true;
                  this.status=true
                  // alert("111")

                },
              error => {
                this.mailSend=false
              })




            }
            if (response.exists !== 'Yes') {
              this.emailError = false;
              this.status=false
            }

          },
          error => {
            this.alertService.error(error);
            this.loading = false;
          });

    }
  else
    {
      this.recaptcha = false;


    }
  }
  ngOnInit() {
  }

}
