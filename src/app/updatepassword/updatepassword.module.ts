import { NgModule } from '@angular/core';
import {CommonModule } from '@angular/common'

import {Resultrouting,appRoutingProviders} from './updatepassword.routes';
import {Ng2PaginationModule} from 'ng2-pagination';
import { FormsModule } from '@angular/forms';
import {UpdatepasswordComponent} from "../updatepassword/updatepassword.component"


import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import {PreloaderModule} from '../components/preloader.module'

@NgModule({
  declarations: [
    UpdatepasswordComponent,

  ],
  imports: [
    CommonModule,

    FormsModule,
    PreloaderModule,
    HttpModule,
    Ng2PaginationModule,
    Resultrouting
  ],

})
export class ResultModule { }
