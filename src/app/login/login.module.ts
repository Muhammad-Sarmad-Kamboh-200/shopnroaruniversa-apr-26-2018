import { NgModule } from '@angular/core';
import {CommonModule } from '@angular/common';
import { RecaptchaModule } from 'ng-recaptcha';
import {LoginComponent} from '../login/login.component';
import {Loginrouting,appRoutingProviders} from './login.routes';
import {Ng2PaginationModule} from 'ng2-pagination';
import { FormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';
import {PreloaderModule} from '../components/preloader.module';

@NgModule({
  declarations: [
    LoginComponent,

  ],
  imports: [
    CommonModule,
    FormsModule,
    PreloaderModule,
    RecaptchaModule.forRoot(),
    HttpModule,
    Ng2PaginationModule,
    Loginrouting
  ],

})
export class LoginModule { }
