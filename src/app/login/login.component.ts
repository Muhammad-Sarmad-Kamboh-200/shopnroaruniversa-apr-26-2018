import {Component, OnInit, Output, EventEmitter, Inject, PLATFORM_ID} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { isPlatformBrowser } from '@angular/common';
import {AlertService, AuthenticationService} from '../_services/index';
import {DataService} from '../shareduser.service';
import {SimpleGlobal} from 'ng2-simple-global';
import swal from 'sweetalert';

@Component({
  selector: 'app-login',
  moduleId: module.id.toString(),
  templateUrl: 'login.component.html',
  providers: [AuthenticationService, AlertService, DataService]
})

export class LoginComponent implements OnInit {
  @Output() value= new EventEmitter<string>();

  model: any = {};

  loading = false;
  returnUrl: string;

  constructor(@Inject(PLATFORM_ID) private platformId: Object,
              private route: ActivatedRoute,
              private router: Router,
              private data: DataService,
              public sgflag: SimpleGlobal,

              private authenticationService: AuthenticationService,
              private alertService: AlertService) {
  }

  sub: any;
  recaptcha :any;


  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response ${captchaResponse}:`);
    if (captchaResponse) {
      this.recaptcha = true;
    }
    // alert('hiii')
  }

  GoogleLogin(){


    console.log('svahdcad  ',this.authenticationService.googlelogin())
    // this.authenticationService.googlelogin().subscribe(params => {
    //   console.log(params);
    //
    //   console.log('Successfull login');
    //   this.router.navigate(['']);
    //
    // }, error => {
    //   console.lologing('Error in service');
    //     this.router.navigate(['/login']);
    //
    //   })
  }
  status:any;
  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {

      window.scrollTo(0, 0)
    }
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '';
    this.sub = this.route.queryParams.subscribe(params => {
      // console.log(params);
      let id = params['username'];
      let token = params['token'];
      console.log(id);
      console.log(token);
      let user = {username: id, token: token};


      if(id!==null && token!=null)
      {
        if (isPlatformBrowser(this.platformId)) {

          localStorage.setItem('currentUser', JSON.stringify(user));
          this.sgflag['login']=true;


          localStorage.setItem('type', JSON.stringify('social login'));
        this.sgflag['flag']=true;
          this.authenticationService.loginID(id).subscribe(
            data => {
              this.loginID = data;
              if (isPlatformBrowser(this.platformId)) {


                this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
                localStorage.setItem('userKey', JSON.stringify(data[0].id));
              }

              console.log(this.loginID)
              this.status=true
              // console.log(this.loginID[0].id)
            }
          )
          this.router.navigate([this.returnUrl]);
        }
      }
      // if (params['ft']) {
      //   // alert(user.username)
      //   localStorage.setItem('currentUser', JSON.stringify(user));
      //
      //
      //   if (user && user.token) {
      //
      //     // store user details and jwt token in local storage to keep user logged in between page refreshes
      //     // localStorage.removeItem('currentUser');
      //
      //
      //     localStorage.setItem('currentUser', JSON.stringify(user));
      //     // localStorage.setItem('profimg', params['img']+'?type=large&access_token='+params['ft'])
      //
      //
      //     // console.log(user)
      //   }
      // }


      //   response.json().token;

    });

    // this.getTwAccessToken();

    // reset login status
    // this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/register';
  }

  loginID: any = []

  verified:any;
  currentUser : any;

  login() {
    this.loading = true;

    this.value.emit('logedin');

    if(this.recaptcha==true)
    {
      this.authenticationService.isUserAuthenticated(this.model.username).subscribe(data=>{
        if(data['code']==true)
        {
          this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
              data => {
                this.router.navigate(['']);
                this.sgflag['login']=true;


                this.authenticationService.loginID(this.model.username).subscribe(
                  data => {
                    this.loginID = data;
                    if (isPlatformBrowser(this.platformId)) {


                      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
                      localStorage.setItem('userKey', JSON.stringify(data[0].id));
                      this.data.currentUser.subscribe(data => this.currentUser = data)
                    }

                    console.log(this.loginID)
                    this.status=true
                    // console.log(this.loginID[0].id)
                  }
                )
              },
              error => {
                this.alertService.error(error);
                this.loading = false;
                this.status=false;
              });
        }
        else {
          this.verified=false;
          swal({
            title: 'Verification Failed?',
            text: 'Your email is not verified yet!',
            icon: 'warning',
            buttons:  [ 'Not Now!','Resend Verification Mail!'],
            dangerMode: true,
          })
            .then((willDelete) => {
              if (willDelete) {
                swal('A verification email has been sent to your account',{
                icon: 'success',
                });
              } else {
                this.authenticationService.resendAuthentication(this.model.username).subscribe(data=>{
                  console.log(data)
                })
              }
            });
        }
      })

    }
    else {
      this.recaptcha=false
    }
  }
}
