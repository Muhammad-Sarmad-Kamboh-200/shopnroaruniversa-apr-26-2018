﻿import { Injectable,  Inject, PLATFORM_ID  } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {HttpService} from '../services/http-service';
import { isPlatformBrowser } from '@angular/common';

import { environment } from '../../environments/environment';

@Injectable()
export class AuthenticationService {
  private url = environment.apiUrl;

  constructor(@Inject(PLATFORM_ID) private platformId: Object,
                private httpserive:HttpService,private http: Http) { }
  loginID(query:string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.httpserive.get(this.url+'user/getID/'+query,{headers: headers}).map(response => response.json());


  }
  userProfile(query:string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.httpserive.get(this.url+'user/getprofile/'+query,{headers: headers}).map(response => response.json());


  }

  isUserAuthenticated(username:any) {
    return this.http.post(this.url+'user/isvarified/',{
      "username":username
    }).map((response: Response) => response.json());
  }

  resendAuthentication(username:any) {
    return this.http.post(this.url+'user/resendAuthentication/',{
      "username":username
    }).map((response: Response) => response.json());
  }

  UpdateProfile(model:any) {
      // alert(model['first_name'])
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.httpserive.post(this.url+'user/updateprofile/', JSON.stringify(model),
      {headers: headers}).map((response: Response) => response);


  }


  // login_service(username: any, pass: any) {
  //   const headers = new Headers();
  //   headers.append('Content-Type', 'application/json');
  //   return this.httpserive.post('http://127.0.0.1:8000/user/login/', {'username': username, 'password': pass})
  //     .map((res: Response) => {
  //       alert(res.status)
  //       if (res) {
  //         if (res.status === 201 || res.status === 200) {
  //           if (isPlatformBrowser(this.platformId)) {
  //             localStorage.setItem('loged_in', '1');
  //             localStorage.setItem('Authorization', res.json().token);
  //           }
  //           // this._nav.navigate([returnUrl]);
  //           //    this.iid= localStorage.getItem('id');
  //           //
  //           //
  //           //    this.Admins_id =  this.jwtHelper.decodeToken(localStorage.getItem('Authorization'))['user_id'];
  //           //
  //           // console.log("ok 201",this.Admins_id);
  //           // this.is_admin( this.Admins_id).subscribe();
  //         } else if (res.status === 7373) {
  //           // this.login=true;
  //
  //           //    this.iid= localStorage.getItem('id');
  //           // console.log(this.iid);
  //           if (isPlatformBrowser(this.platformId)) {
  //             localStorage.setItem('loged_in', '1');
  //           }
  //           // console.log('ok 200');
  //           // console.log('responecechecker', res.json());
  //
  //           // console.log(res.json().token);
  //           if (isPlatformBrowser(this.platformId)) {
  //             localStorage.setItem('Authorization', res.json().token);
  //           }
  //           // this.Admins_id =  this.jwtHelper.decodeToken(localStorage.getItem('Authorization'))['user_id'];
  //           // console.log("ok 200",this.Admins_id);
  //
  //           // this.is_admin( this.Admins_id).subscribe();
  //           // this._nav.navigate(['/home']);
  //         }
  //       }
  //     }).catch((error: any) => {
  //       if (error.status === 404) {
  //         if (isPlatformBrowser(this.platformId)) {
  //           localStorage.setItem('loged_in', '0');
  //         }
  //         // console.log(' not 1');
  //         //   this._nav.navigate(['/login']);
  //         return Observable.throw(new Error(error.status));
  //       } else if (error.status === 400) {
  //         if (isPlatformBrowser(this.platformId)) {
  //           localStorage.setItem('loged_in', '0');
  //         }
  //         // console.log(' not 2');
  //         // this._nav.navigate(['/login']);
  //         return Observable.throw(new Error(error.status));
  //       } else if (error.status === 401) {
  //         if (isPlatformBrowser(this.platformId)) {
  //           localStorage.setItem('loged_in', '0');
  //         }
  //         // console.log(' not 3');
  //         // console.log('ok not submited submite');
  //         //   this._nav.navigate(['/login']);
  //       } else {
  //         // console.log(' not 4');
  //         if (isPlatformBrowser(this.platformId)) {
  //           localStorage.setItem('loged_in', '0');
  //         }
  //         // this._nav.navigate(['/login']);
  //         return Observable.throw(new Error(error.status));
  //       }
  //     });
  // }

  login(username: string, password: string) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.httpserive.post(this.url+'user/user-token-auth/',
      { username: username, password: password }, {headers: headers})
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        let user =  { username: username, token: response.json().token};

        if (user && user.token) {

          if (isPlatformBrowser(this.platformId)) {

            localStorage.setItem('currentUser', JSON.stringify(user));
            //     // console.log(user)
          }
        }
      });
  }





  googlelogin() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.httpserive.get(this.url+'oauth/login/google-oauth2/',{headers: headers})
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        alert(response)


      });
  }

  logout() {
    if (isPlatformBrowser(this.platformId)) {

      // remove user from local storage to log user out
      localStorage.removeItem('User');
      localStorage.removeItem('token');

      localStorage.removeItem('currentUser');
      return this.http.get(this.url+'user/api-token-refresh').map(response => response.json());
    }

    }
}
