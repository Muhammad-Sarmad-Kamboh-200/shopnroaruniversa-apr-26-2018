import {Component, OnInit, AfterViewInit, Inject, PLATFORM_ID} from '@angular/core';
// import { ICarouselConfig, AnimationConfig } from 'angular4-carousel';
import {HttpService} from '../http.service';
import {isPlatformBrowser} from '@angular/common';
import {Http} from '@angular/http';
import {environment} from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

declare var $: any;
@Component({
  selector: 'app-home1',
  templateUrl: './home1.component.html',
  styleUrls: ['./home1.component.css'],
  providers: [HttpService]

})
export class Home1Component implements OnInit,AfterViewInit {
  private url = environment.apiUrl;

  // @ViewChild('owlElement') owlElement: OwlCarousel;
  public imagesUrl: any = [];
  title: any;
  public images: string[] = [
    '../../assets/images/slider/5.jpg',
    '../../assets/images/slider/4.jpg',
    '../../assets/images/slider/3.jpg',
    '../../assets/images/slider/5.jpg',
    '../../assets/images/slider/2.jpg',
    '../../assets/images/slider/6.jpg',
    '../../assets/images/slider/5.jpg',
    '../../assets/images/slider/2.jpg',
    '../../assets/images/slider/6.jpg'
  ];

  public brands = [{
    image: '../../assets/media/logo/header2/newblack.jpg',
    name: 'Hot Deals',
    link: 'all'
  }, {
    image: 'https://storage.shopnroar.com/EBay.png',
    name: 'Hot Deals',
    link: 'Ebay'
  },
    {
      image: 'https://storage.shopnroar.com/Walmart.png',
      name: 'Hot Deals',
      link: 'Walmart'
    },
    {
      image: 'https://storage.shopnroar.com/groupon.png',
      name: 'Hot Deals',
      link: 'Groupon'
    }, {
      image: 'https://storage.shopnroar.com/bestbuylogo.png',
      name: 'Hot Deals',
      link: 'BestBuy'
    }, {
      image: 'https://storage.shopnroar.com/buydiglogo.png',
      name: 'Hot Deals',
      link: 'BuyDig'
    }, {
      image: 'https://storage.shopnroar.com/amazon.png',
      name: 'Hot Deals',
      link: 'Amazon'
    }, {
      image: 'https://storage.shopnroar.com/kmartlogo.png',
      name: 'Hot Deals',
      link: 'Kmart'
    }, {
      image: 'https://storage.shopnroar.com/fry.png',
      name: 'Hot Deals',
      link: 'FRYS'
    }, {
      image: 'https://storage.shopnroar.com/bh.png',
      name: 'Hot Deals',
      link: 'BHPHOTOVIDEO'
    },
  ]
  searchflag = false;
  recomflag = false;
  specialday = false;

  constructor(private httpService: HttpService,private http_c: HttpClient,
              @Inject(PLATFORM_ID) private platformId: Object, private http: Http,) {
  }

  public products: any = [];
  public Allresults: any = [];
  i = 0;
  public recommendations: any = [];
  public SpecialDayresults: any = [];

  ngAfterViewInit(){

    this.imagesUrl.push('https://bmj2k.files.wordpress.com/2011/04/heroes.jpg');
    this.imagesUrl.push('http://www.reuun.com/data/out/57/402924406-girls-wallpapers.jpg')

    this.httpService.gethotdeals(1).subscribe(
      data => {
        this.products = data;
        console.log(this.products);

      }
    );


    if (isPlatformBrowser(this.platformId)) {

      var searchhistory = JSON.parse(localStorage.getItem('searchedItem')) || 0;
      console.log(searchhistory)
      if (searchhistory != null || searchhistory!== 0) {
        setTimeout(() => {
          this.searchGeneric(searchhistory, -1, -1, -1, -1, -1, 1).subscribe(
            data => {
              this.searchflag = true;
              this.Allresults = (data)

            }
          );
        }, 10)

        this.title = JSON.parse(localStorage.getItem('Recommendation')) || 0;
        console.log(this.title)

        if(this.title!==0){

          var naam=this.title.split(' ');
          this.i = 1;
          var recom = this.title[0]

          if(naam.length<5){
            while (this.i < naam.length) {
              recom = recom + ' ' + naam[this.i];
              this.i = this.i + 1;
            }

          }
          if(naam.length>5){

            while (this.i < naam.length/3) {
              recom = recom + ' ' + naam[this.i];
              this.i = this.i + 1;
            }
          }

          console.log(recom)
          setTimeout(() => {
            this.searchGeneric(recom, -1, -1, -1, -1, -1, 1).subscribe(
              data => {
                this.recomflag = true;
                this.recommendations = (data)

              }
            );
          }, 12)
        }
      }
      else {

      }
    }

  }
  ngOnInit() {


  }

  searchGeneric(query: string, lower: any, upper: any, brand: any, merchant: any, category: any, page: any) {
    let headers = new Headers();
    // alert(query)
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'search/filterallproducts/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/' + category + '/?page=' + page).map(response => response.json());

  }


  // fun() {
  //   this.owlElement.next([200])
  //   // alert('next')
  //   //duration 200ms
  // }


}
