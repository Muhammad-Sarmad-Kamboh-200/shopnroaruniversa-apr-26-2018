import {HttpService} from '../http.service';
import {Http} from '@angular/http';
import {Component, AfterViewInit, OnInit, Inject, PLATFORM_ID, ChangeDetectorRef, ElementRef, OnChanges} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {isPlatformBrowser} from '@angular/common';
import {environment} from '../../environments/environment';

import {DataService} from '../shareduser.service';
import {SimpleGlobal} from 'ng2-simple-global';
import {Subject} from 'rxjs/Subject';
import {HttpClient} from '@angular/common/http';
import { DataSharedService } from "../shareddata.service";
import {Observable} from "rxjs/Observable";
declare var $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [HttpService, DataService,DataSharedService]
})
export class HeaderComponent implements OnInit, AfterViewInit,OnChanges {

  currentUser: any;
  Usertype: any;
  private url = environment.apiUrl;
  catCount: any;

  public filterkeyUp = new Subject<string>();

  public countries: any = [];
  public listTitles = [];
  public tempTitles = [];
  public TitlesL = [];
  public filteredList = [];
  public elementRef;
  public flag = false;
  public socialflag = false;
  public Category = [];
  message:string;
  catsIsCalled=false;
  called = false;

  page: any;

  constructor(@Inject(PLATFORM_ID) private platformId: Object,
              private data: DataService,
              public sgflag: SimpleGlobal,
              private dataShared: DataSharedService,
              private httpC: HttpClient,
              private http: Http, private httpService: HttpService, myElement: ElementRef, private router: Router, private ref: ChangeDetectorRef, private activatedRoute: ActivatedRoute) {

    this.elementRef = myElement;


    this.filterkeyUp
      .map(event => event)
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(search => Observable.of(search).delay(300))
      .subscribe(data => {
        //console.log('observe  ',data);
        this.filter(data);
      });
  }

  query: string;
  user: any;
  toogleFlag = false;

  toogle() {
    this.toogleFlag = !this.toogleFlag;
  }

  navigatetohome() {
    this.router.navigate(['']);

  }

  getAllCategories() {

    return this.http.get(this.url + 'search/GetAllCategories').map(response => response.json());

  }

  verifylogin() {
    if (isPlatformBrowser(this.platformId)) {

      var currentUser = JSON.parse(localStorage.getItem('currentUser')) || 0;
      var localtoken = currentUser.token; // your token
      this.user = currentUser.username;

      return this.http.post(this.url + 'user/api-token-verify/', {'token': localtoken})
        .map(response => {
          const token = response.json() && response.json().token;
          if (token) {
            this.flag = true;
            return true;
          } else {
            this.flag = false;
            return false;
          }
        });
    }
  }


  logout() {
    if (isPlatformBrowser(this.platformId)) {

      this.flag = false;
      this.socialflag = false;
      // remove user from local storage to log user out
      var currentUser = JSON.parse(localStorage.getItem('currentUser')) || 0;
      var localtoken = currentUser.token; // your token
      // window.open("http://ns519750.ip-158-69-23.net:8005/settings/logout");

      // remove user from local storage to log user out
      localStorage.removeItem('currentUser');
      localStorage.removeItem('type');
      localStorage.removeItem('userKey');
      this.currentUser = null;
      this.flag = false;
      return this.http.post(this.url + 'user/api-token-refresh/', {'token': localtoken})
        .map(response => {
          const token = response.json() && response.json().token;
          localStorage.removeItem('loginUser');
          localStorage.removeItem('currentUser');
          localStorage.removeItem('logintoken');

        });

    }
  }

  setflag() {
// //console.log('asmas')
    if (isPlatformBrowser(this.platformId)) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser')) || 0;
      this.Usertype = JSON.parse(localStorage.getItem('type')) || 0;

      if (this.currentUser != null)
        this.flag = true;
      else
        this.flag = false;
      if (this.currentUser == 0)
        this.flag = false;
      if (this.Usertype != null) {
        this.socialflag = true;

        this.sgflag['flag'] = true;
      }
      if (this.Usertype == 0) {
        this.sgflag['flag'] = false;
        this.socialflag = false;
      }
//console.log(this.flag)

    }
  }


  ngOnInit() {

    // this.httpC.get('https://www.googleapis.com/customsearch/v1?key=AIzaSyBo0J96Ie4hZ_xfpGdFt8u8hFJyJWYnsvQ&cx=017576662512468239146:omuauf_lfve&q=dall latitude').subscribe(data=>{
    //   // //console.log(data)
    //   try {
    //     //console.log(data['spelling']['correctedQuery'])
    //
    //   }
    //   catch {
    //     //console.log('already correct')
    //
    //   }
    //   // alert('data')
    // })

    if (isPlatformBrowser(this.platformId)) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser')) || 0;
      this.Usertype = JSON.parse(localStorage.getItem('type')) || 0;

      if (this.currentUser != null)
        this.sgflag['login'] = true;
      else
        this.sgflag['login']= false;
      if (this.currentUser == 0)
        this.sgflag['login'] = false;

      if (this.Usertype != null) {
        this.socialflag = true;

        this.sgflag['flag'] = true;
      }
      if (this.Usertype == 0) {
        this.sgflag['flag'] = false;
        this.socialflag = false;
      }
      //console.log(this.flag)

    }

    // alert(this.sgflag['login'])
    // this.Category = [
    //   'Entertainment',
    //   'Tv & Video',
    //   'Sports',
    //   'Art & Collectibles',
    //   'Food & Grocery',
    //   'Electronics',
    //   'Uncategorized',
    //   'Health, Fitness & Beauty',
    //   'Women\'S Shoes',
    //   'Security, Surveillance & Safety',
    //   'Home & Garden',
    //   'Office & Business',
    //   'Amazon Exclusives',
    //   'Computers & Accessories',
    //   'Camera Drones',
    //   'Toys, Kids & Babies',
    //   'Furniture',
    //   'Kindle & Fire Tablets',
    //   'Dining Furniture Sets',
    //   'Computers',
    //   'Computer Graphics Cards',
    //   'Tv, Video, & Audio',
    //   'Deals',
    //   'Cameras & Camcorders',
    //   'Movies, Music & Games',
    //   'Boys\' Shoes',
    //   'Computer & Laptops',
    //   'Books & Audible',
    //   'Sporting Goods',
    //   'Network & Connectivity',
    //   'Travel & Occasions',
    //   'Mobile Apps',
    //   'Wearables',
    //   'Amazon Launchpad',
    //   'Grocery, Household & Pet',
    //   'Fashion',
    //   'Vehicle & Gps',
    //   'Shoes',
    //   'Cellphones & Accessories',
    //   'Smart Home',
    //   'Computer Softwares',
    //   'Walmart For Business',
    //   'Books',
    //   'Appliances',
    //   'Laptops & Netbooks',
    //   'Jewelry & Watches',
    //   'Video Games & Consoles',
    //   'Tools'
    // ];
    this.callcategories()

    this.sgflag['Category'] = this.Category;
    localStorage.setItem('Categories', JSON.stringify(this.Category))


  }

  ngOnChanges(){


  }

  ngAfterViewInit() {


  }

  callcategories() {
    // alert('trying..');


    if (this.catsIsCalled === false) {
      this.catsIsCalled=true;
          // alert('calling');
          this.httpC.get(this.url+'search/GetAllCategories').subscribe(
            data => {


              this.Category = data['Categories'];
              this.catsIsCalled = true;

              this.sgflag['Category'] = data['Categories'];
              // //console.log(data['Categories'])

              localStorage.setItem('Categories', JSON.stringify(data));

            },error2 => {
              this.catsIsCalled=false;
            }
          );



    }

  }

  Suggestions(query: string, page: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.get(this.url + 'search/suggestions/' + query + '/?page=' + page).map(response => response.json());

  }

  refreshlist() {
    this.listTitles = [];

  }

  filter(query) {
    if (query !== '') {
      // //console.log('query......... ' + query);
      this.Suggestions(query, 1).subscribe(
        data => {
          this.countries = data.results;
          this.listTitles = [];

          for (let item of this.countries) {
            // //console.log(item.SNR_Title)
            this.listTitles.push(item.SNR_Title);
            // this.countries.add(item.SNR_Title)

          }
          // //console.log(this.listTitles)
          // //console.log(this.countries)
        });

      this.filteredList = this.listTitles.filter(function (el) {
        return el.toLowerCase().indexOf(this.query) > -1;

      }.bind(this));
      // //console.log(this.filteredList)
    } else {
      this.filteredList = [];
    }
  }

  select(item) {
    this.query = item;
    this.Search();
  }

  filterquery: any;

  Search() {

    if (isPlatformBrowser(this.platformId)) {

      localStorage.setItem('searchedItem', JSON.stringify(this.query));
    }
    this.filterquery = this.query;
    this.query = null;
    this.listTitles = null;
    // alert(this.query)

    $('#mainModel').css('display', 'none');
    // $('.modal-backdrop.in').css('opacity', 0);
    // $('.modal-backdrop.in').css('display', 'none');
    // $('body').removeClass('modal-open')
    $('div').removeClass('modal-backdrop fade in');
    $('#closebtn').click();

    this.listTitles = this.tempTitles;

    var re =/[!@#"$%^&*'\\\/]/g;
    this.filterquery=this.filterquery.replace(re,'')

    // alert(this.filterquery)
    this.router.navigate(['/result', this.filterquery, 'general']);
  }

  modelshow() {
    setTimeout(function () {
      $('#querytosearch').focus();
    }, 500);
    setTimeout(function () {
      $('#querytosearch').focus();
    }, 1200);
  }

  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
  }
}
