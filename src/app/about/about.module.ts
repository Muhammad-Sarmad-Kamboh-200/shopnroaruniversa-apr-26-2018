import { NgModule } from '@angular/core';
import {CommonModule } from '@angular/common';

import {AboutComponent} from "../about/about.component";
import {Abouttrouting,appRoutingProviders} from './about.routes';
import {Ng2PaginationModule} from 'ng2-pagination';
import { FormsModule } from '@angular/forms';
// import { PreloaderService } from '../services/preloader-service';
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import {PreloaderModule} from '../components/preloader.module'
import {PartnerModule} from '../partners/partners.module'

// export function httpServiceFactory(backend: XHRBackend, defaultOptions: RequestOptions, preloaderService: PreloaderService) {
//   return new HttpService(backend, defaultOptions, preloaderService);
// }

@NgModule({
  declarations: [
    AboutComponent,

  ],
  imports: [
    CommonModule,
    PartnerModule,
    FormsModule,
    PreloaderModule,
    HttpModule,
    Ng2PaginationModule,
    Abouttrouting
  ],

})
export class AboutModule { }
