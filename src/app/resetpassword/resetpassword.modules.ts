import { NgModule } from '@angular/core';
import {CommonModule } from '@angular/common'

import {ResetpasswordComponent} from "../resetpassword/resetpassword.component"
import {Resultrouting,appRoutingProviders} from './resetpassword.routes';
import {Ng2PaginationModule} from 'ng2-pagination';
import { FormsModule } from '@angular/forms';


import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import {PreloaderModule} from '../components/preloader.module'

@NgModule({
  declarations: [
    ResetpasswordComponent,

  ],
  imports: [
    CommonModule,

    FormsModule,
    PreloaderModule,
    HttpModule,
    Ng2PaginationModule,
    Resultrouting
  ],

})
export class ResultModule { }
