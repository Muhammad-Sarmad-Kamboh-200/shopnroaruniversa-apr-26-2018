import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import {HttpService} from '../http.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css'],
  providers: [HttpService]

})
export class ResetpasswordComponent implements OnInit {

  model: any = {}
  sub: any;
  link: any;

  constructor(private route: ActivatedRoute,
              private httpService: HttpService,
              private router: Router) {

  }

  updated: any;
  match: any;

  update() {
    if (this.model.password === this.model.repeatpassword) {
      this.sub = this.route.params.subscribe(params => {
        this.link = 'http://www.shopnroar.com/reset/' + params['id']; // (+) converts string 'id' to a number

        this.httpService.Check_link(this.link).subscribe((data) => {
          console.log(data)


          this.email = (data[0].email)

          console.log(data[0].isValid)

          let time = new Date();
          let time1 = new Date(data[0].SNR_Date);


          if (time.getTime() - time1.getTime() < 300000) {
            this.httpService.UpdatePassword(this.email, this.model.password).subscribe((res) => {
              this.updated = true
              this.router.navigate(['/login']);
            })
          }
          else {
            this.timepass = false

          }

        })

      })


    }
  }

  timepass: any;

  check() {

    if (this.model.password == this.model.repeatpassword) {
      this.match = true
    }
    else {
      this.match = false
    }

  }

  linkfound: any;

  email: any;
  id: any
  myTime: any;

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      this.link = "http://www.shopnroar.com/reset/" + params['id'];

      this.httpService.Check_link(this.link).subscribe((data) => {
        if (data.length == 0) {
          this.router.navigate(['']);
        }
      }, error => {

        this.router.navigate(['']);

      });
      if (!this.id) {
        this.router.navigate(['']);
      }

    })

  }
}
