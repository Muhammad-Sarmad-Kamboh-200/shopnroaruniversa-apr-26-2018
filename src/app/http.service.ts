import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {HttpService as mainservice} from './services/http-service';
import {environment} from '../environments/environment';

@Injectable()
export class HttpService {

  private url = environment.apiUrl;

  constructor(private http: mainservice) {
  }

  // check_email_unique(email: any) {
  //   return this.http.post( 'http://127.0.0.1:8000/user/email_verify/',{
  //     email:email
  //   }).map((response: Response) => response.json());
  // }
  getAllCategories() {

    return this.http.get(this.url + 'search/GetAllCategories').map(response => response.json());

  }
  getAllTVS(page: any) {

    return this.http.get(this.url + 'products/tv?page=' + page).map(response => response.json());

  }

  gethotdeals(page: any) {

    return this.http.get(this.url + 'products/todayhotdeals?page=' + page).map(response => response.json());

  }
  getdealsCategories(page: any) {

    return this.http.get(this.url + 'products/dealsCategories?page=' + page).map(response => response.json());

  }
getdailydeals(page: any) {


    return this.http.get(this.url + 'products/todaydeals?page=' + page).map(response => response.json());

  }


  getAllTVSASC(page: any) {

    return this.http.get(this.url + 'products/tvASC?page=' + page).map(response => response.json());

  }

  getAllTVSDESC(page: any) {

    return this.http.get(this.url + 'products/tvDESC?page=' + page).map(response => response.json());

  }

  getAllCams(page: any) {

    return this.http.get(this.url + 'products/cams?page=' + page).map(response => response.json());

  }

  getAllCamsASC(page: any) {

    return this.http.get(this.url + 'products/camsASC?page=' + page).map(response => response.json());

  }

  getAllCamsDESC(page: any) {

    return this.http.get(this.url + 'products/camsDESC?page=' + page).map(response => response.json());

  }


  getAllCounts() {

    return this.http.get(this.url + 'search/count').map(response => response.json());

  }

  getAllTrendsCount() {

    return this.http.get(this.url + 'trend/getCount').map(response => response.json());

  }

  getAllAppliences(page: any) {

    return this.http.get(this.url + 'products/applinces?page=' + page).map(response => response.json());

  }

  getAllHealth(page: any) {

    return this.http.get(this.url + 'products/health?page=' + page).map(response => response.json());

  }

  getAllHealthASC(page: any) {

    return this.http.get(this.url + 'products/healthASC?page=' + page).map(response => response.json());

  }

  getAllHealthDESC(page: any) {

    return this.http.get(this.url + 'products/healthDESC?page=' + page).map(response => response.json());

  }

  getAllAppliencesDESC(page: any) {

    return this.http.get(this.url + 'products/applincesDESC?page=' + page).map(response => response.json());

  }

  getAllAppliencesASC(page: any) {

    return this.http.get(this.url + 'products/applincesASC?page=' + page).map(response => response.json());

  }

  getAllAudio(page: any) {

    return this.http.get(this.url + 'products/audio?page=' + page).map(response => response.json());

  }


  getAllAudioASC(page: any) {

    return this.http.get(this.url + 'products/audioASC?page=' + page).map(response => response.json());

  }

  getAllAudioDESC(page: any) {

    return this.http.get(this.url + 'products/audioDESC?page=' + page).map(response => response.json());

  }

  getAllCarsElectronics(page: any) {

    return this.http.get(this.url + 'products/carelectronics?page=' + page).map(response => response.json());

  }


  getAllCarsElectronicsDESC(page: any) {

    return this.http.get(this.url + 'products/carelectronicsDESC?page=' + page).map(response => response.json());

  }

  getAllCarsElectronicsASC(page: any) {

    return this.http.get(this.url + 'products/carelectronicsASC?page=' + page).map(response => response.json());

  }

  getAllVideogames(page: any) {

    return this.http.get(this.url + 'products/videogames?page=' + page).map(response => response.json());

  }


  getAllVideogamesASC(page: any) {

    return this.http.get(this.url + 'products/videogamesASC?page=' + page).map(response => response.json());

  }

  getAllVideogamesDESC(page: any) {

    return this.http.get(this.url + 'products/videogamesDESC?page=' + page).map(response => response.json());

  }


  getAllOffice(page: any) {

    return this.http.get(this.url + 'products/office?page=' + page).map(response => response.json());

  }

  getAllOfficeASC(page: any) {

    return this.http.get(this.url + 'products/officeASC?page=' + page).map(response => response.json());

  }

  getAllOfficeDESC(page: any) {

    return this.http.get(this.url + 'products/officeDESC?page=' + page).map(response => response.json());

  }


  getAllSmarthome(page: any) {

    return this.http.get(this.url + 'products/smarthome?page=' + page).map(response => response.json());

  }


  getAllSmarthomeASC(page: any) {

    return this.http.get(this.url + 'products/smarthomeASC?page=' + page).map(response => response.json());

  }


  getAllSmarthomeDESC(page: any) {

    return this.http.get(this.url + 'products/smarthomeDESC?page=' + page).map(response => response.json());

  }


  getAllSoftware(page: any) {

    return this.http.get(this.url + 'products/software?page=' + page).map(response => response.json());

  }

  getAllSoftwareASC(page: any) {

    return this.http.get(this.url + 'products/softwareASC?page=' + page).map(response => response.json());

  }

  getAllSoftwareDESC(page: any) {

    return this.http.get(this.url + 'products/softwareDESC?page=' + page).map(response => response.json());

  }


  getAllToys(page: any) {

    return this.http.get(this.url + 'products/toys?page=' + page).map(response => response.json());

  }

  getAllToysASC(page: any) {

    return this.http.get(this.url + 'products/toysASC?page=' + page).map(response => response.json());

  }

  getAllToysDESC(page: any) {

    return this.http.get(this.url + 'products/toysDESC?page=' + page).map(response => response.json());

  }

  getAllMovies(page: any) {

    return this.http.get(this.url + 'products/movies?page=' + page).map(response => response.json());

  }

  getAllMoviesaASC(page: any) {

    return this.http.get(this.url + 'products/moviesASC?page=' + page).map(response => response.json());

  }

  getAllMoviesDESC(page: any) {

    return this.http.get(this.url + 'products/moviesDESC?page=' + page).map(response => response.json());

  }


  getAllMobiles(page: any) {

    return this.http.get(this.url + 'mobile/viewAll?page=' + page).map(response => response.json());

  }


  getAllMobilesASC(page: any) {

    return this.http.get(this.url + 'mobile/viewAllASC?page=' + page).map(response => response.json());

  }

  getAllMobilesDESC(page: any) {

    return this.http.get(this.url + 'mobile/viewAllDESC?page=' + page).map(response => response.json());

  }

  getAllLaptops(page: any) {

    return this.http.get(this.url + 'laptop/?page=' + page).map(response => response.json());

  }


  getAllArts(page: any) {

    return this.http.get(this.url + 'products/arts?page=' + page).map(response => response.json());

  }

  getAllArtsASC(page: any) {

    return this.http.get(this.url + 'products/artsASC?page=' + page).map(response => response.json());

  }

  getAllArtsDESC(page: any) {

    return this.http.get(this.url + 'products/artsDESC?page=' + page).map(response => response.json());

  }


  getClothesArts(page: any) {

    return this.http.get(this.url + 'products/clothes?page=' + page).map(response => response.json());

  }

  getAllClothesASC(page: any) {

    return this.http.get(this.url + 'products/clothesASC?page=' + page).map(response => response.json());

  }

  getAllClothesDESC(page: any) {

    return this.http.get(this.url + 'products/clothesDESC?page=' + page).map(response => response.json());

  }


  gethomegarden(page: any) {

    return this.http.get(this.url + 'products/homegarden?page=' + page).map(response => response.json());

  }

  getAllhomegardenASC(page: any) {

    return this.http.get(this.url + 'products/homegardenASC?page=' + page).map(response => response.json());

  }

  getAllhomegardenDESC(page: any) {

    return this.http.get(this.url + 'products/homegardenDESC?page=' + page).map(response => response.json());

  }


  getAllFurniture(page: any) {

    return this.http.get(this.url + 'products/furniture?page=' + page).map(response => response.json());

  }

  getAllFurnitureASC(page: any) {

    return this.http.get(this.url + 'products/furnitureASC?page=' + page).map(response => response.json());

  }

  getAllFurnitureDESC(page: any) {

    return this.http.get(this.url + 'products/furnitureDESC?page=' + page).map(response => response.json());

  }

  getAlljewelry(page: any) {

    return this.http.get(this.url + 'products/jewelry?page=' + page).map(response => response.json());

  }

  getAlljewelryASC(page: any) {

    return this.http.get(this.url + 'products/jewelryASC?page=' + page).map(response => response.json());

  }

  getAlljewelryDESC(page: any) {

    return this.http.get(this.url + 'products/jewelryDESC?page=' + page).map(response => response.json());

  }


  getAllflowers(page: any) {

    return this.http.get(this.url + 'products/flowers?page=' + page).map(response => response.json());

  }

  getAllflowersASC(page: any) {

    return this.http.get(this.url + 'products/flowersASC?page=' + page).map(response => response.json());

  }

  getAllflowersDESC(page: any) {

    return this.http.get(this.url + 'products/flowersDESC?page=' + page).map(response => response.json());

  }

  getAllsports(page: any) {

    return this.http.get(this.url + 'products/sports?page=' + page).map(response => response.json());

  }

  getAllsportsASC(page: any) {

    return this.http.get(this.url + 'products/sportsASC?page=' + page).map(response => response.json());

  }

  getAllsportsDESC(page: any) {

    return this.http.get(this.url + 'products/sportsDESC?page=' + page).map(response => response.json());

  }


  getAllBooks(page: any) {

    return this.http.get(this.url + 'products/books?page=' + page).map(response => response.json());

  }

  getAllBooksASC(page: any) {

    return this.http.get(this.url + 'products/booksASC?page=' + page).map(response => response.json());

  }

  getAllBooksDESC(page: any) {

    return this.http.get(this.url + 'products/booksDESC?page=' + page).map(response => response.json());

  }


  getAllGadgets(page: any) {

    return this.http.get(this.url + 'products/gadgets/?page=' + page).map(response => response.json());

  }

  getAllGadgetsASC(page: any) {

    return this.http.get(this.url + 'products/gadgetsASC/?page=' + page).map(response => response.json());

  }

  getAllGadgetsDESC(page: any) {

    return this.http.get(this.url + 'products/gadgetsDESC/?page=' + page).map(response => response.json());

  }


  getAllLaptopsASC(page: any) {

    return this.http.get(this.url + 'laptop/sortASC?page=' + page).map(response => response.json());

  }

  getAllLaptopsDESC(page: any) {

    return this.http.get(this.url + 'laptop/sortDESC?page=' + page).map(response => response.json());

  }

  getWearables(page: any) {

    return this.http.get(this.url + 'wearable/?page=' + page).map(response => response.json());

  }

  getWearablesASC(page: any) {

    return this.http.get(this.url + 'wearable/sortASC?page=' + page).map(response => response.json());

  }

  getWearablesDESC(page: any) {

    return this.http.get(this.url + 'wearable/sortDESC?page=' + page).map(response => response.json());

  }

  searchGeneric(query: string, lower: any, upper: any, brand: any, merchant: any,category:any, page: any, preloaderType: any) {

    console.log(query)
    let headers = new Headers();

    console.log(lower)
    console.log(upper)
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'search/filterallproducts/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant+ '/' + category  + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  searchGenericbyModel(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'search/filterbyModel/' + query + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  searchGenericbyModelASC(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'search/filterbyModelASC/' + query + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  sendLinkMail(email: string, link: string) {

    return this.http.post(this.url + 'user/email/',
      {
        'email': email,

        'sharedlink': link
      }).map((res: Response) => {
      if (res) {
        if (res.status === 201 || res.status === 200) {
          console.log('ok submited');
        }
        else {
          console.log(res.status)

        }
      }
    }).catch((error: any) => {
      console.log(error);
      if (error.status !== 404) {
        if (error.status === 401) {
          console.log(error);

          return Observable.throw(new Error(error.status));
        }


      } else {
        console.log(error);
        //   this._nav.navigate(['/login']);

        return Observable.throw(new Error(error.status));
      }
    });

  }


  Addwishlist(data: any) {
    var UserID = JSON.parse(localStorage.getItem('userKey'));

    return this.http.post(this.url + 'feedback/addwishlist', {
      "user": UserID,
      "SNR_SKU": data['SNR_SKU'],
      "SNR_Title": data['SNR_Title'],
      "SNR_ModelNo": data['SNR_ModelNo'],
      "SNR_Brand": data['SNR_Brand'],
      "SNR_UPC": data['SNR_UPC'],
      "SNR_Price": data['SNR_Price'],
      "SNR_ProductURL": data['SNR_ProductURL'],
      "SNR_Available": data['SNR_Available'],
      "SNR_Description": data['SNR_Description'],
      "SNR_ImageURL": data['SNR_ImageURL'],
      "SNR_CustomerReviews": data['SNR_CustomerReviews'],
    }).map((res: Response) => {
      if (res) {
        if (res.status === 201 || res.status === 200) {
          // console.log('ok submited');
        }
        else {
          console.log(res.status)

        }
      }
    }).catch((error: any) => {
      // //alert(error.status);
      if (error.status !== 404) {
        if (error.status === 401) {
          console.log(error);
          alert('This item is already added in your favourite list')


          return Observable.throw(new Error(error.status));
        }


      }
      if (error.status == 500 || error.status == 501) {
        alert('This item is already added in your favourite list')

      } else {
        console.log(error);
        //   this._nav.navigate(['/login']);

        return Observable.throw(new Error(error.status));
      }
    });

  }


  searchGenericbyModelDESC(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'search/filterbyModelDESC/' + query + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  searchGenericASC(query: string, lower: any, upper: any, brand: any, merchant: any,category:any, page: any, preloaderType: any) {
    // alert(query)
    let headers = new Headers();
    console.log(lower)
    console.log(upper)
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'search/filterallproductsASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant+ '/' + category + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  wishlistbyUser(page: any) {
    var UserID = JSON.parse(localStorage.getItem('userKey'));

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'feedback/wishlistbyuser/' + UserID + '/?page=' + page, {headers: headers}).map(response => response.json());

  }

  searchGenericDESC(query: string, lower: any, upper: any, brand: any, merchant: any,category:any, page: any, preloaderType: any) {
    let headers = new Headers();
    console.log(lower)
    console.log(upper)
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'search/filterallproductsDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant+ '/' + category  + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  searchGrouponGeneric(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'search/filtergroupon/' + query + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  searchGrouponGenericASC(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'search/filtergrouponASC/' + query + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  searchGrouponGenericDESC(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'search/filtergrouponDESC/' + query + '/?page=' + page, null, preloaderType).map(response => response.json());
  }


  getTrends(page: any, preloaderType: any) {

    return this.http.get(this.url + 'trend/getTrends?page=' + page, null, preloaderType).map(response => response.json());

  }

  getTrendsMobile(page: any, preloaderType: any) {

    return this.http.get(this.url + 'trend/getTrendsMobile?page=' + page, null, preloaderType).map(response => response.json());

  }

  getTrendsLaptop(page: any, preloaderType: any) {

    return this.http.get(this.url + 'trend/getTrendsLaptop?page=' + page, null, preloaderType).map(response => response.json());

  }


  getTrendsGame(page: any, preloaderType: any) {

    return this.http.get(this.url + 'trend/getTrendsgame?page=' + page, null, preloaderType).map(response => response.json());

  }

  getTrendsMovie(page: any, preloaderType: any) {

    return this.http.get(this.url + 'trend/getTrendsmovie?page=' + page, null, preloaderType).map(response => response.json());

  }

  getTrendsAudio(page: any, preloaderType: any) {

    return this.http.get(this.url + 'trend/getTrendsaudio?page=' + page, null, preloaderType).map(response => response.json());

  }


  getTrendsCars(page: any, preloaderType: any) {

    return this.http.get(this.url + 'trend/getTrendscar?page=' + page, null, preloaderType).map(response => response.json());

  }

  getTrendsApliances(page: any, preloaderType: any) {

    return this.http.get(this.url + 'trend/getTrendsAppliances?page=' + page, null, preloaderType).map(response => response.json());

  }

  getTrendsWearable(page: any, preloaderType: any) {

    return this.http.get(this.url + 'trend/getTrendsWearable?page=' + page, null, preloaderType).map(response => response.json());

  }

  getTrendsTV(page: any, preloaderType: any) {

    return this.http.get(this.url + 'trend/getTrendstv?page=' + page, null, preloaderType).map(response => response.json());

  }

  getTrendsCams() {

    return this.http.get(this.url + 'trend/getTrendscam').map(response => response.json());

  }


  getTrendsToy(page: any, preloaderType: any) {

    return this.http.get(this.url + 'trend/getTrendstoys?page=' + page, null, preloaderType).map(response => response.json());

  }

  getpopular(page: any) {

    return this.http.get(this.url + 'trend/popular?page=' + page).map(response => response.json());

  }


  getStoreMobiles() {

    //  return this.http.get('https://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=huzaifaz-ERecomme-PRD-008fa563f-8f5d310e&RESPONSE-DATA-FORMAT=JSON&keywords='+model+' plus&sortOrder=PricePlusShippingLowest').map(response => response.json());

    let headers = new Headers();
    headers.set('Access-Control-Allow-Origin', "*");
    headers.append('Content-Type', 'application/json');
    //alert("calling");
    return this.http.get('https://svcs.ebay.com/MerchandisingService?OPERATION-NAME=getTopSellingProducts&SERVICE-NAME=MerchandisingService&SERVICE-VERSION=1.1.0&CONSUMER-ID=huzaifaz-ERecomme-PRD-008fa563f-8f5d310e&RESPONSE-DATA-FORMAT=JSON&REST-PAYLOAD&maxResults=3').map(response => response.json());


  }

  getOwnData() {
    return this.http.get('https://192.168.1.34:8000/login/check/?format=json');
  }

/////////////

  ///////////>> ADD-Mobiles <</////////////
  Add_Mobiles(user: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.url + 'mobile/AddMobile', JSON.stringify(user),
      {headers: headers}).map((response: Response) => response.json());


  }

  Add_Review(user: string, pid: string, star: any, title: string,  review: string, ) {
    console.log(user)
    console.log(pid)
    console.log(star)
    console.log(title)
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.url + 'products/addreview/', {
        "SNR_Review_Title":title,
        "SNR_Review_Author" :user,
        "SNR_Review_Body" : review,
        "SNR_Review_Stars" :star,
        "Product" : pid
      },
      {headers: headers}).map((response: Response) =>
      response.json()
    );


  }

  Add_link(email: string, link: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.url + 'user/reset/', {
      "email": email,
      "link": link,
      "isValid": false,
      "Code": null,

    }, {headers: headers}).map((response: Response) => response.json());


  }

  Check_link(link: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.url + 'user/checkreset/', {

        "link": link
      },
      {headers: headers}).map((response: Response) => response.json());


  }

  UpdatePassword(email: any, password: any) {
    //alert(email)
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.url + 'user/updatepassword/' + email + '/' + password,
      {headers: headers}).map((response: Response) => response.json());


  }

  UpdatePasswordmanually(username: any, password: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.url + 'user/updatepasswordmanually/', {
        username: username,
        password: password
      },
      {headers: headers}).map((response: Response) => response.json());


  }


  Add_Feedback(name: string, email: string, title: string, feedback: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.url + 'feedback/add', {
        "SNR_FullName": name,
        "SNR_Email": email,
        "SNR_Subject": title,
        "SNR_Feedback": feedback,
      },

      {headers: headers}).map((response: Response) => response.json());


  }


  Add_Subscriber(email: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.url + 'user/subscribers/add', {
        "email": email,
        "isSend": "false"
      },

      {headers: headers}).map((response: Response) => {
        response.json()
        console.log(response)
        if (response.status === 200 || response.status === 201) {
          //alert("good")

        }

      }
    ).catch((error: any) => {
      console.log(error);

      if (error.status !== 404) {
        if (error.status === 400) {
          //alert("Email already exists")

        }
        if (error.status === 401) {
          console.log(error);

          return Observable.throw(new Error(error.status));
        }


      } else {
        console.log(error);
        //   this._nav.navigate(['/login']);

        return Observable.throw(new Error(error.status));
      }
    });


  }



  FilterProductsbyCategory(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/category/' + query + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  getGoogle(query: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get('https://www.googleapis.com/customsearch/v1?key=AIzaSyBo0J96Ie4hZ_xfpGdFt8u8hFJyJWYnsvQ&cx=017576662512468239146:omuauf_lfve&q=lectures').map(response => response.json());

  }

  FilterProductsbyCategoryASC(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/category_ASC/' + query + '?page=' + page, null, preloaderType).map(response => response.json());

  }
  ProductsComparison(query: string, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'search/filterproductComparison/' + query , null, preloaderType).map(response => response.json());

  }
  FilterProductsbyCategoryDESC(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/category_DESC/' + query + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterLaptop(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'laptop/filterlaptop/' + query + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterGamesbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtergamesbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }
  FilterDeals(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertodaydeals/' + query + '/'  + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterGamesDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtergamesDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterGamesASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtergamesASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterAudiobyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filteraudiobyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterAudioASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filteraudioASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterAudioDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filteraudioDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterBooksbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterbooksbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }
  Filterdealsbybrand(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertodaydealsbybrand/' + query + '/' + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterBooksASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterbooksASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterBooksDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterbooksDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterFlowersbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterflowersbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterFlowersASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterflowersASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterFlowersDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterflowersDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterLaptopbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'laptop/filterlaptopbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterLaptopDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'laptop/filterlaptopDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterLaptopASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'laptop/filterlaptopASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterWearablebyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'wearable/filterbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterWearableASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'wearable/filterASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterWearableDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'wearable/filterDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterMobilebyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'mobile/filtermobilebyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterMobilebASCyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'mobile/filtermobileASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterMobileDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'mobile/filtermobileDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterHealthbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterhealthbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterHealthASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterhealthASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterHealthDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterhealthDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterApplincesbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterappliencesbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterApplincesDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterappliencesDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterApplincesASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterappliencesASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  Filtertvbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertvbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FiltertvASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertvASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FiltertvDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertvDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  Filtertoysbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertoysbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FiltertoysASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertoysASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FiltertoysDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertoysDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterSoftwarebyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtersoftwarebyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterSoftwareASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtersoftwareASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterSoftwareDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtersoftwareDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterArtsbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterartsbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterSportsbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtersportsbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterSportsASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtersportsASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterSportsDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtersportsDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterGeneralbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'search/filterbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterGeneralASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'search/filterbypriceASC/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterGeneralDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'search/filterbypriceDESC/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterFurniturebyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterfurniturebyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterFurnitureASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterfurnitureASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterFurnitureDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterfurnitureDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterJewelrybyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterjewelrybyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterJewelryASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterjewelryASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterJewelryDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterjewelryDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterHomegardenbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterhomegardenbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterHomegardenASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterhomegardenASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterHomegardenDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterhomegardenDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterClothesbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterclothesbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterClothesASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterclothesASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterClothesDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterclothesDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterFlowerbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterflowersbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterFlowerASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterflowersASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterFlowerDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterflowersDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterArtsASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterartsASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterArtsDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterartsDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterOfficesupplyDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterofficesupplyDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterOfficesupplyASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterofficesupplyASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterOfficesupplybyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterofficesupplybyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterSmarthomebyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtersmarthomesbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterSmarthomeDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtersmarthomesDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterSmarthomeASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtersmarthomesASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  Filtercamsbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtercamsbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FiltercamsASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtercamsASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FiltercamsDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtercamsDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterMoviesbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtermoviesbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterMoviesASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtermoviesASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterMoviesDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtermoviesDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  Filtercarselecbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtercarselectronicsbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FiltercarselecASCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtercarselectronicsASCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }


  FiltercarselecDESCbyprice(query: string, price1: any, price2: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtercarselectronicsDESCbyprice/' + query + '/' + price1 + '/' + price2 + '?page=' + page, null, preloaderType).map(response => response.json());

  }



  FilterArts(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterartssall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }
  FilterArtsASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterartssallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }
  FilterArtsDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterartssallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterSports(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtersportssall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterSportsASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtersportssallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterSportsDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtersportssallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterFurniture(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterfurnituresall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterFurnitureASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterfurnituresallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterFurnitureDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterfurnituresallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterJewelry(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterjewelrysall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterJewelryASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterjewelrysallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterJewelryDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterjewelrysallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterHomegarden(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterhomegardensall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterHomegardenASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterhomegardensallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterHomegardenDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterhomegardensallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterClothes(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterclothessall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterClothesASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterclothessallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }
  FilterClothesDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterclothessallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }



  FilterFlowersASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterflowerssallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }
  FilterFlowers(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterflowerssall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterFlowersDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterflowerssallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterBooks(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterbookssall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterBooksASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterbookssallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterBooksDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterbookssallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  checkpasswordExistence(username: string, password: string) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.url + 'user/verifypassword/',
      {username: username, password: password}, {headers: headers})
      .map(response => response.json());
  }


  getReview(query: string, page: any) {
    let headers = new Headers();
    // alert(query)
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'review/filterReview/' + query + '/?page=' + page).map(response => response.json());

  }

  getVendorReview(query: string, page: any) {
    let headers = new Headers();
    // alert(query)
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'review/filterReviewVendor/' + query + '/?page=' + page).map(response => response.json());

  }


  FilterLaptopASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'laptop/filteralllaptposASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page='  + page, null, preloaderType).map(response => response.json());

  }


  FilterHealthASC(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterhealthASC/' + query + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterHealth(query: string, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterhealth/' + query + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

    FilterHealthDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
      return this.http.get(this.url + 'products/filterhealthnfitnessallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page,  null, preloaderType).map(response => response.json());

    }

  FilterLaptopDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'laptop/filteralllaptposDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page='  + page, null, preloaderType).map(response => response.json());

  }

  FilterWearable(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'wearable/filterallwearable/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }



  FilterWearableASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'wearable/filterallwearableASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterWearableDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'wearable/filterallwearableDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  CountbyQuery(query: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'search/countQuery/' + query).map(response => response.json());

  }


  FilterMobile(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'mobile/filterallmobile/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page,  null, preloaderType).map(response => response.json());

  }

  FilterMobileASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'mobile/filterallmobileASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page,  null, preloaderType).map(response => response.json());

  }


  FilterMobileDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'mobile/filterallmobileDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page,  null, preloaderType).map(response => response.json());

  }


  FilterAppliances(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterappliencesall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterAppliancesDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterappliencesallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterAppliancesASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filterappliencesallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterGames(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtergamessallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterGamesASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtergamessallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }



  FilterGameDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtergamessallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }




  FilterTVASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertvsallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }




  FilterTV(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertvsall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterTVDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertvsallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterToys(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertoyssall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterToysASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertoyssallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterToysDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtertoyssallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterSoftware(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtersoftwaresall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }



  FilterSoftwareASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
//console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'products/filtersoftwaresallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterSoftwareDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtersoftwaresallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterOfficesupply(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterofficesupplysall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterOfficesupplyASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterofficesupplysallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterOfficesupplyDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filterofficesupplysallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterHome(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtersmarthomesall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterHomeASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtersmarthomesallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterHomeDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtersmarthomesallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterCams(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtercamssall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterCamsASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtercamssallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }



  FilterCamsDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtercamssallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterMovies(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtermoviessall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterMoviesASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtermoviessallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterMoviesDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtermoviessallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterCarsElec(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtercarselecsall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterCarsElecASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtercarselecsallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }

  FilterCarsElecDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filtercarselecsallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterAudio(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filteraudiosall/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterAudioASC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filteraudiosallASC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }


  FilterAudioDESC(query: string, lower: any, upper: any, brand: any, merchant: any, page: any, preloaderType: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.url + 'products/filteraudiosallDESC/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant + '/?page=' + page, null, preloaderType).map(response => response.json());

  }



  private handleError(error: any) {
    console.log(error);
    return Observable.throw(error);
  }
}




