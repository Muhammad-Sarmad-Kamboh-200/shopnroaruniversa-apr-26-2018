import { NgModule } from '@angular/core';
import {CommonModule } from '@angular/common'

import {TorsComponent} from '../tors/tors.component'
import {Ng2PaginationModule} from 'ng2-pagination';
import { FormsModule } from '@angular/forms';
import {Detailrouting,appRoutingProviders} from './tors.routes';

import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import {PreloaderModule} from '../components/preloader.module'
@NgModule({
  declarations: [
    TorsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    PreloaderModule,
    HttpModule,
    Ng2PaginationModule,
    Detailrouting
  ],

})
export class ResultModule { }
