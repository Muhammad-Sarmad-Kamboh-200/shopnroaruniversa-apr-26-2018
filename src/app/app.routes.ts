import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {AllmobilesComponent} from './allmobiles/allmobiles.component';
import {MobilesComponent} from './mobiles/mobiles.component'
import {WishlistuserComponent} from './wishlistuser/wishlistuser.component'
import {UpdateprofileComponent} from './updateprofile/updateprofile.component';
import {AuthGuard} from "./auth-gard/auth-gard.component";

import { ProductDetailsComponent } from './product-details/product-details.component';
import {Home1Component} from "./home1/home1.component";

export const routes: Routes = [
  {
    path: 'login',
    loadChildren: 'app/login/login.module#LoginModule'
  },
  {
    path: 'hotdeals',
    loadChildren: 'app/hotdeals/hotdeals.module#LoginModule'
  },
  {
    path: 'deals/:id',
    loadChildren: 'app/deals/deals.module#LoginModule'
  },
  {
    path: 'contact',
    loadChildren: 'app/contact/contact.module#ContactModule'
  },
  {
    path: 'result/:id/:cat',
    component: MobilesComponent
  },


  {
    path: 'policy',
    loadChildren: 'app/privacypolicy/privacypolicy.module#ResultModule'
  },

  {
    path: 'termsofcondition',
    loadChildren: 'app/tors/tors.module#ResultModule'
  },
  {
    path: 'details',
    component: ProductDetailsComponent
  },
  {
    path: 'view/:id',
    component: AllmobilesComponent
  },
  // {path:'about' , component:AboutComponent},

  {
    path: 'reset/:id',
    loadChildren: 'app/resetpassword/resetpassword.modules#ResultModule'
  },
  {
    path: 'updatepassword',
    loadChildren: 'app/updatepassword/updatepassword.module#ResultModule',
    canActivate: [AuthGuard]

  },
  {
    path: 'forgotpassword',
    loadChildren: 'app/forgotpassword/forgotpassword.module#ForgotModule'
  },
  {
    path: 'register',
    loadChildren: 'app/register/register.module#ResultModule'
  },
  {
    path: 'about',
    loadChildren: 'app/about/about.module#AboutModule'
  },{
    path: 'howshopnroarworks',
    loadChildren: 'app/howitwork/howitwork.module#WorkModule'
  },
  {
    path: 'myshopnroar',
    loadChildren: 'app/wishlistuser/wishlistuser.module#ResultModule',
    canActivate: [AuthGuard]

  },
  {
    path: 'profile',
    loadChildren: 'app/updateprofile/updateprofile.module#ResultModule',
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: Home1Component
  },
  // {
  //   path: '',
  //   loadChildren: 'app/home/home.module#HomeModule'
  // },

  // {path: '**', redirectTo: '/error404'}
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
