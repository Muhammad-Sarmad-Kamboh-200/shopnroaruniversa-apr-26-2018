import { Component, OnInit,Inject, PLATFORM_ID  } from '@angular/core';
import { HttpService } from '../http.service';
import { Http} from '@angular/http';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
       providers: [HttpService]
})
export class ContactComponent implements OnInit {
data:any=[]
  constructor(@Inject(PLATFORM_ID) private platformId: Object,private httpService: HttpService,   private http: Http) {

   }

  flag=false;

  title:any;
  url:any
  flaag=false;
  user:any;
  model: any = {};
  sendfeedback(){
    this.httpService.Add_Feedback(this.model.full_name,this.model.email,this.model.subject,this.model.feedback).subscribe(

      this.model.full_name=null,
      this.model.email=null,
      this.model.subject=null


  );
this.model.feedback=null;
    this.flaag=true
  }


  ngOnInit() {
    // this.verifylogin().subscribe()
    if (isPlatformBrowser(this.platformId)) {

      window.scrollTo(0, 0)

    }
  }
}
