import { NgModule } from '@angular/core';
import {CommonModule } from '@angular/common'
import {ContactComponent} from "../contact/contact.component"
import {Contactrouting,appRoutingProviders} from './contact.routes';
import {Ng2PaginationModule} from 'ng2-pagination';
import { FormsModule } from '@angular/forms';

import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import {PreloaderModule} from '../components/preloader.module'

@NgModule({
  declarations: [
    ContactComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    PreloaderModule,
    HttpModule,
    Ng2PaginationModule,
    Contactrouting
  ],

})
export class ContactModule { }
