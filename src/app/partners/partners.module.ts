import {CommonModule } from '@angular/common'

import { NgModule } from '@angular/core';
import {PartnersComponent } from '../partners/partners.component'

@NgModule({
  declarations: [
    PartnersComponent

  ],
  imports: [
    CommonModule
  ],
  exports:[
    PartnersComponent
  ]

})
export class PartnerModule { }
