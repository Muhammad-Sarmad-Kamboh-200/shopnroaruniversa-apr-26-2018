import {Component, ChangeDetectorRef, Inject, PLATFORM_ID} from '@angular/core';
import {HttpService} from '../http.service';
import {Router, ActivatedRoute,} from '@angular/router';
import {Http} from '@angular/http';
import {isPlatformBrowser} from '@angular/common';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-mobiles',
  templateUrl: './mobiles.component.html',
  styleUrls: ['./mobiles.component.css'],
  providers: [HttpService]
})

export class MobilesComponent {
  private url = environment.apiUrl;

  options = [{'name': 'Best Match'}, {'name': 'Price: low to high'}, {'name': 'Price: high to low'}];
  selectedOption = this.options[0]

  merchants = [{'name': 'Best Buy'}, {'name': 'Walmart'}, {'name': 'Ebay'}, {'name': 'Amazon'}, {'name': 'Groupon'}, {'name': 'Newsegg'}];

  sort: string = "Best Match";
  id;
  pageresult: any;
  lower: any;
  upper: any;
  brandresult: any;
  merchant: any;
  category: any;

  sub: any;
  objectKeys = Object.keys;
  constructor(@Inject(PLATFORM_ID) private platformId: Object,
              private router: Router,
              private http: Http,
              private httpC: HttpClient,
              private httpService: HttpService, private route: ActivatedRoute, private ref: ChangeDetectorRef) {

    this.pageresult = 1;
    this.merchant = -1;
    this.lower = -1;
    this.upper = -1;
    this.category = -1;
    this.brandresult = -1;
    //console.log(this.lower)
  }
  categoryChanged(cat){
    if(cat=='null'){
      this.category=-1;

    }
    else{
      this.category=cat;

    }
    this.Allmerchants=[]
    this.AllBrands=[]
    setTimeout(() => {

      this.CountBrandsbyQuery(this.query, this.lower, this.upper, this.brandresult, this.merchant,this.category).subscribe(
        data => {

          this.Allmerchants = (data['merchants'])
          this.AllBrands = (data['brands'])
          //console.log(this.AllCount)
          // this.isFinish = true
        }
      );
    }, 0)

    this.Search();
  }

  public Getspecs: any[] = [];
  count: number = 0;
  query: string;
  p: any;

  pageno: any;

  searchedQuery: string;
  productpricelow: any;
  productpricehigh: any;

  public GetLaptops: any[] = [];
  Popular: any = []

  loadmorebrands() {
    this.brandcount = this.brandcount + 10;
  }

  onChangeOption(event) {

    this.sort = event.name;
    this.pageno = 1;
    this.pageresult = this.pageno;
    this.isFinish = false;
    ////alert('vvv'+this.sort)
    this.AllSearch()
  }

  FilterbyMerchants(item) {
    this.merchant = item;
    this.AllSearch()
  }

  AllSearch() {
    this.Allresults=[]
    this.errorflag=false;
    {
      if (this.sort == "Best Match") {

        this.pageno = 1;
        if (this.cat == "general") {
          this.count = 0
          this.httpService.searchGeneric(this.query, this.lower, this.upper, this.brandresult, this.merchant,this.category, this.pageresult, 'full').subscribe(
            data => {

              this.Allresults = (data['results'])

              this.count = data['totalItems'];
              // ////alert('sd')
              this.errorflag=false;

              ////this.isFinish=true
            },
            error => {
              //console.log('sdfsdfsdfsdfsdfsd   ', error.status)
              this.errorflag=true;
            }
          );


        }

      }

      else if (this.sort == "Price: low to high") {
        this.Allresults = []

        this.pageno = 1;
        if (this.cat == "general") {
          this.httpService.searchGenericASC(this.query, this.lower, this.upper, this.brandresult, this.merchant,this.category, this.pageresult, 'full').subscribe(
            data => {

              this.Allresults = (data['results'])
              this.count = data['totalItems'];
              // ////alert('sd')

              ////this.isFinish=true
            },
            error => {
              //console.log('sdfsdfsdfsdfsdfsd   ', error.status)
              this.errorflag=true;
            }
          );

        }


      }
      else if (this.sort == "Price: high to low") {
        this.pageno = 1;
        this.Allresults = []
        if (this.cat == "general") {
          this.count = 0
          this.httpService.searchGenericDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant,this.category, this.pageresult, 'full').subscribe(
            data => {

              this.Allresults = (data['results'])
              this.count = data['totalItems'];
              // ////alert('sd')
              this.errorflag=false;

              ////this.isFinish=true
            },
            error => {
              //console.log('sdfsdfsdfsdfsdfsd   ', error.status)
              this.errorflag=true;
            }
          );


        }
      }

    }
  }

  onClickSearch() {
    this.query = this.searchedQuery;
    this.httpService.searchGenericASC(this.query, this.lower, this.upper, this.brandresult, this.merchant,this.category, this.pageresult, 'full').subscribe(
      data => {
        this.Allresults = data['results'];
        this.count = data['totalItems'];
        ////////console.log(data)
        this.ref.detectChanges();
        this.ref.reattach();
      },
      error => {
        //console.log('sdfsdfsdfsdfsdfsd   ', error.status)
        this.errorflag=true;
      }
    );

  }

  pageChanged(event) {
    this.isFinish = false
    this.p = event;

    this.pageno = event
    //////alert(this.sort)

    this.Allresults = []
    if (this.sort == "Best Match") {

      this.pageno = this.pageno;
      if (this.cat == "general") {
        this.count = 0
        this.httpService.searchGenericDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant,this.category, this.pageno, 'full').subscribe(
          data => {

            this.Allresults = (data['results'])

            this.count = data['totalItems'];
            // ////alert('sd')

            ////this.isFinish=true
          },
          error => {
            //console.log('sdfsdfsdfsdfsdfsd   ', error.status)
            this.errorflag=true;
          }
        );


      }
      else if (this.cat == "mobile") {
        this.httpService.FilterMobileDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.count = data['totalItems']
            //this.isFinish=true
            ////////console.log(this.Getspecs)
            // ////////console.log(data)
            //this.isFinish=true
          }
        );

      }


      else if (this.cat == "furniture") {
        this.httpService.FilterFurnitureDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "sports") {
        this.httpService.FilterSportsDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "health") {
        this.httpService.FilterHealthDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "wearable") {
        this.httpService.FilterWearableDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])

            this.num = data['totalItems']
            this.cou = 0
            //this.isFinish=true
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "health") {
        this.httpService.FilterHealthDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "flowers") {
        this.httpService.FilterFlowersDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "arts") {
        this.httpService.FilterArtsDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "homegarden") {
        this.httpService.FilterHomegardenDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "jewelry") {
        this.httpService.FilterJewelryDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "clothes") {
        this.httpService.FilterClothesDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "books") {
        this.httpService.FilterBooksDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "laptop") {
        this.httpService.FilterLaptopDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "audio") {
        this.httpService.FilterAudioDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "appliances") {
        this.httpService.FilterAppliancesDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              //this.isFinish=true
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "cam") {
        // ////////alert(("hi"))
        this.httpService.FilterCamsDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "cars") {
        this.httpService.FilterCarsElecDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              //this.isFinish=true
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "games") {
        this.httpService.FilterGameDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "home") {
        this.httpService.FilterHomeDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "movie") {
        this.httpService.FilterMoviesDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;
              //this.isFinish=true
            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "software") {
        this.httpService.FilterSoftwareDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "office") {
        this.httpService.FilterOfficesupplyDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)
              //this.isFinish=true
              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "toys") {
        this.httpService.FilterToysDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "tv") {
        this.httpService.FilterTVDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

    }

    else if (this.sort == "Price: low to high") {
      this.pageno = this.pageno;

      if (this.cat == "general") {
        this.httpService.searchGenericASC(this.query, this.lower, this.upper, this.brandresult, this.merchant,this.category, this.pageno, 'full').subscribe(
          data => {

            this.Allresults = (data['results'])
            this.count = data['totalItems'];
            // ////alert('sd')

            ////this.isFinish=true
          },
          error => {
            //console.log('sdfsdfsdfsdfsdfsd   ', error.status)
            this.errorflag=true;
          }
        );

      }
      else if (this.cat == "mobile") {
        this.httpService.FilterMobileASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "wearable") {
        this.httpService.FilterWearableASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "laptop") {
        this.httpService.FilterLaptopASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "audio") {
        this.httpService.FilterAudioASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "appliances") {
        this.httpService.FilterAppliancesASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "cam") {
        this.httpService.FilterCamsASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "cars") {
        this.httpService.FilterCarsElecASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "games") {
        this.httpService.FilterGamesASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "home") {
        this.httpService.FilterHomeASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "movie") {
        this.httpService.FilterMoviesASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "software") {
        this.httpService.FilterSoftwareASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "office") {
        this.httpService.FilterOfficesupplyASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "toys") {
        this.httpService.FilterToysASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "tv") {
        this.httpService.FilterTVASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = data['results'];
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "flowers") {
        this.httpService.FilterFlowersASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "arts") {
        this.httpService.FilterArtsASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "homegarden") {
        this.httpService.FilterHomegardenASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "furniture") {
        this.httpService.FilterFurnitureASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "sports") {
        this.httpService.FilterSportsASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "jewelry") {
        this.httpService.FilterJewelryASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "clothes") {
        this.httpService.FilterClothesASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "books") {
        this.httpService.FilterBooksASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }


    }
    else if (this.sort == "Price: high to low") {
      this.pageno = this.pageno;
      this.Allresults = []
      if (this.cat == "general") {
        this.count = 0
        this.httpService.searchGenericDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant,this.category, this.pageno, 'full').subscribe(
          data => {

            this.Allresults = (data['results'])
            this.count = data['totalItems'];
            // ////alert('sd')

            ////this.isFinish=true
          },
          error => {
            //console.log('sdfsdfsdfsdfsdfsd   ', error.status)
            this.errorflag=true;
          }
        );

      }
      else if (this.cat == "mobile") {
        this.httpService.FilterMobileDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.count = data['totalItems']
            //this.isFinish=true
            ////////console.log(this.Getspecs)
            // ////////console.log(data)
            //this.isFinish=true
          }
        );

      }


      else if (this.cat == "health") {
        this.httpService.FilterHealthDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "wearable") {
        this.httpService.FilterWearableDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])

            this.num = data['totalItems']
            this.cou = 0
            //this.isFinish=true
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "health") {
        this.httpService.FilterHealthDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "flowers") {
        this.httpService.FilterFlowersDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "arts") {
        this.httpService.FilterArtsDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "homegarden") {
        this.httpService.FilterHomegardenDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "furniture") {
        this.httpService.FilterFurnitureDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "sports") {
        this.httpService.FilterSportsDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "jewelry") {
        this.httpService.FilterJewelryDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "clothes") {
        this.httpService.FilterClothesDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "books") {
        this.httpService.FilterBooksDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            //this.isFinish=true
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "laptop") {
        this.httpService.FilterLaptopDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "audio") {
        this.httpService.FilterAudioDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "appliances") {
        this.httpService.FilterAppliancesDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              //this.isFinish=true
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "cam") {
        // ////////alert(("hi"))
        this.httpService.FilterCamsDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "cars") {
        this.httpService.FilterCarsElecDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              //this.isFinish=true
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            // ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "games") {
        this.httpService.FilterGameDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

      else if (this.cat == "home") {
        this.httpService.FilterHomeDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }
            //this.isFinish=true
            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "movie") {
        this.httpService.FilterMoviesDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;
              //this.isFinish=true
            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "software") {
        this.httpService.FilterSoftwareDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "office") {
        this.httpService.FilterOfficesupplyDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)
              //this.isFinish=true
              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "toys") {
        this.httpService.FilterToysDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }
      else if (this.cat == "tv") {
        this.httpService.FilterTVDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.pageno, 'full').subscribe(
          data => {
            this.Allresults = (data['results'])
            this.num = data['totalItems']
            this.cou = 0
            // if(this.num>this.count)
            {
              ////////alert(this.num+this.count)

              this.count = this.num;

            }

            ////////console.log(data)
            this.ref.detectChanges();
            this.ref.reattach();
          }
        );

      }

    }


  }


  searchbyCat(event) {
    // ////////alert("hi")
    //  ////////console.log(event);
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.srcElement.attributes.id;
    var value = idAttr.nodeValue;
    // ////////alert(value)
    // ////////alert(this.query);
    // this.query = value;
  }

  onClick(event) {
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
  }

  AllCount: any = [];
  regexp = new RegExp('^<(\\w+)>|<\\/(\\w+)>$')
  Allresults = [];
  Allmerchants = [];
  AllBrands = [];
  brandcount = 10;
  isFinish = false;
  num: number;
  cou: number;


  FilterbyBrands(brand) {
    this.brandresult = brand;
    this.errorflag=false;
    this.AllSearch()

  }

  errorflag=false;

  Search() {


      this.errorflag=false;
      this.isFinish = false


        this.count = 0

    //alert(this.query)
    if (this.sort == "Best Match") {
      this.httpService.searchGeneric(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.category, this.pageresult, 'full').subscribe(
        data => {

          //console.log('data..................' + data)

          this.Allresults = (data['results'])
          //console.log((this.Allresults))

          this.count = data['totalItems'];

          // ////alert('sd')
          this.errorflag = false;

          this.isFinish = true
        },
        error => {
          this.Allresults = []
          //console.log('sdfsdfsdfsdfsdfsd   ', error.status)
          this.errorflag = true;
        }
      );

    }

if (this.sort == "Price: high to low") {
        // //alert("asd")
      this.httpService.searchGenericDESC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.category, this.pageresult, 'full').subscribe(
        data => {

          //console.log('data..................' + data)

          this.Allresults = (data['results'])
          //console.log((this.Allresults))

          this.count = data['totalItems'];

          // ////alert('sd')
          this.errorflag = false;

          this.isFinish = true
        },
        error => {
          this.Allresults = []
          //console.log('sdfsdfsdfsdfsdfsd   ', error.status)
          this.errorflag = true;
        }
      );

    }

if (this.sort == "Price: low to high") {
      this.httpService.searchGenericASC(this.query, this.lower, this.upper, this.brandresult, this.merchant, this.category, this.pageresult, 'full').subscribe(
        data => {

          //console.log('data..................' + data)

          this.Allresults = (data['results'])
          //console.log((this.Allresults))

          this.count = data['totalItems'];

          // ////alert('sd')
          this.errorflag = false;

          this.isFinish = true
        },
        error => {
          this.Allresults = []
          //console.log('sdfsdfsdfsdfsdfsd   ', error.status)
          this.errorflag = true;
        }
      );

    }


  }

  pagetogo = 1;

  loadmore(event) {

    this.pagetogo = this.pagetogo + 1;
    this.httpService.getpopular(this.pagetogo).subscribe(
      data => {
        const myArray = [];


        this.Popular = data
      }
    );
    this.ref.detectChanges();
    this.ref.reattach();

  }


// onClick(event) {
//     ////////console.log(event);
//     ////////console.log(event.srcElement.attributes.id);
//     var idAttr = event.srcElement.attributes.id;
//     var value = idAttr.nodeValue;
//      localStorage.setItem('divValue', JSON.stringify(value));
//  //  ////////alert(value);
// }
  cat: any;
  flag = false;
  title: any;
  user: any;
  AllCatCount: any = [];


  listviewflag = true;
  gridviewflag = false;

  listview() {
    this.listviewflag = true;
    this.gridviewflag = false;
  }

  gridview() {
    this.listviewflag = false;
    this.gridviewflag = true;
  }

  productfilter = false;

  ProductPrice(low: any, high: any) {
    this.isFinish = false;


    this.productpricelow = low;
    this.productpricehigh = high;

    this.lower = low;
    this.upper = high;

    this.AllSearch()

  }

  spellcorrection:any;
  ngOnInit() {

    // this.verifylogin().subscribe()
    if (isPlatformBrowser(this.platformId)) {

      window.scrollTo(0, 0)
    }
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      this.httpC.get('https://www.googleapis.com/customsearch/v1?key=AIzaSyBo0J96Ie4hZ_xfpGdFt8u8hFJyJWYnsvQ&cx=017576662512468239146:omuauf_lfve&q='+this.id).subscribe(data=>{
        try {

          this.spellcorrection=true;
          this.query=(data['spelling']['correctedQuery'])
          // //alert(this.query)
          this.Search();

          this.CountbyQuery(this.query).subscribe(
            data => {

              this.AllCount = data;
              //console.log(this.AllCount)

              //console.log(this.objectKeys(this.AllCount))
            }
          );

        }
        catch {
          this.spellcorrection=false;

//console.log('already corrected')
          this.query = this.id;
          this.Search();

          this.CountbyQuery(this.query).subscribe(
            data => {

              this.AllCount = data;
              //console.log(this.AllCount)

              //console.log(this.objectKeys(this.AllCount))
            }
          );

        }
        // //alert('data')
      })


      this.cat = params['cat'];
      //  ////////alert(this.cat)
//  ////////alert(this.query)
      this.isFinish = false
      this.selectedOption = this.options[0]
      // this.cat="mobile"
      this.isFinish = false
     this.errorflag=false;



      setTimeout(() => {

        this.CountBrandsbyQuery(this.query, this.lower, this.upper, this.brandresult, this.merchant,this.category).subscribe(
          data => {

            this.Allmerchants = (data['merchants'])
            this.AllBrands = (data['brands'])
            //console.log(this.AllCount)
            // this.isFinish = true
          }
        );
      }, 200)
      setTimeout(() => {
        this.httpService.getTrendsToy(1, 'full').subscribe(
          data => {


            this.Popular = data
          }
        );
      }, 300)

    });



  }

  CountbyQuery(query: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
////console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'search/countQuery/' + query).map(response => response.json());

  }


  CountBrandsbyQuery(query: string, lower: any, upper: any, brand: any, merchant: any,category:any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
////console.log('https://localhost:8000/mobile/filtermobile/'+query)
    return this.http.get(this.url + 'search/CountallproductsBrandMerchants/' + query + '/' + lower + '/' + upper + '/' + brand + '/' + merchant+ '/' + category).map(response => response.json());

  }

  private ngOnDestroy() {
    // ////////alert("no")
    this.sub.unsubscribe();
  }
  setQuery(){
    this.query=this.id;
    this.Search()
  }
}
