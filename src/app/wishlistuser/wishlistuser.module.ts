import { NgModule } from '@angular/core';
import {CommonModule } from '@angular/common'

import {Resultrouting,appRoutingProviders} from './wishlistuser.routes';
import {Ng2PaginationModule} from 'ng2-pagination';
import { FormsModule } from '@angular/forms';
import {WishlistuserComponent} from "../wishlistuser/wishlistuser.component"


import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import {PreloaderModule} from '../components/preloader.module'

@NgModule({
  declarations: [
    WishlistuserComponent,

  ],
  imports: [
    CommonModule,

    FormsModule,
    PreloaderModule,
    HttpModule,
    Ng2PaginationModule,
    Resultrouting
  ],

})
export class ResultModule { }
