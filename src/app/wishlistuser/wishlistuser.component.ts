import {Component, OnInit, Inject, PLATFORM_ID} from '@angular/core';
import {HttpService} from '../http.service';
import {isPlatformBrowser} from '@angular/common';
import {Router} from '@angular/router';
import { SimpleGlobal } from 'ng2-simple-global';

@Component({
  selector: 'app-wishlistuser',
  templateUrl: './wishlistuser.component.html',
  styleUrls: ['./wishlistuser.component.scss'],
  providers: [HttpService]

})
export class WishlistuserComponent implements OnInit {
  public products: any[] = [];
  public Popular: any[] = [];
  listview = false;
  gridview = true;

  constructor(@Inject(PLATFORM_ID) private platformId: Object,
              public sgflag: SimpleGlobal,
              private httpService: HttpService, private router: Router) {
  }

  pageChanged(event) {
    // alert(event)

    this.httpService.wishlistbyUser(event).subscribe(
      data => {
        this.products = data;
        console.log(this.products)
      }
    );

  }

  logout() {
    if (isPlatformBrowser(this.platformId)) {

      this.sgflag['login']=false;

      // remove user from local storage to log user out
      var currentUser = JSON.parse(localStorage.getItem('currentUser')) || 0;
      var localtoken = currentUser.token; // your token
      // window.open("http://ns519750.ip-158-69-23.net:8005/settings/logout");

      // remove user from local storage to log user out
      localStorage.removeItem('currentUser');
      localStorage.removeItem('type');
      localStorage.removeItem('userKey');
      setTimeout(() => this.router.navigate(['']), 300);

    }
  }

  listviewfun() {
    this.listview = true;
    this.gridview = false;
  }

  gridviewfun() {
    this.listview = false;
    this.gridview = true;
  }


  ngOnInit() {
    this.httpService.getTrendsGame(1, 'full').subscribe(
      data => {


        this.Popular = data
      }
    );

    this.httpService.wishlistbyUser(1).subscribe(
      data => {
        this.products = data;
        console.log(this.products)
      }
    );

  }

}
