import {Component, OnInit, Inject, PLATFORM_ID , ChangeDetectorRef} from '@angular/core';
import {HttpService} from '../http.service';
import {Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import {
  Http} from '@angular/http';
import { isPlatformBrowser } from '@angular/common';
import {environment} from '../../environments/environment';
import {SimpleGlobal} from 'ng2-simple-global';

import { Chart } from 'angular-highcharts';
@Component({
  selector: 'app-allmobiles',
  templateUrl: './allmobiles.component.html',
  styleUrls: ['./allmobiles.component.css'],
  providers: [HttpService]
})
export class AllmobilesComponent implements OnInit {

  private urlapi = environment.apiUrl;



  reviewCount=10;
  errorflag:any;


  current=80;
  max=5;

  overallrating=2.5;
  overallStarrating=60/20;
  // vendorSinglereviews: review;
  graph=false;
  options = [{'name': 'Best Match'}, {'name': 'Price: low to high'}, {'name': 'Price: high to low'}];
  selectedOption = this.options[0]
  sort: any;
  chartCategories=[]
  positiveseries=[]
  negativeseries=[]
  neutralseries=[]
  // chartCategories=['General','Price','Weight']

  chart : any;





  color = 'primary';
  colorwarn = 'warn';
  mode = 'determinate';
  value = 80;

  percentage= -25;
  objectKeys = Object.keys;
  // options = [{'name': 'Best Match'}, {'name': 'Price: low to high'}, {'name': 'Price: high to low'}];
  // selectedOption = this.options[1]
  count = 0;
  sub: any;
  listview=false;
  gridview=true;
  apiflag=false;

  searchedQuery: string;
  show: any;
  // sort: any;
  modelNo: any;
  query: string;
  path = 'wearable.jpg';
  title = 'app works!';
  Popular: any = [];
  Categories: any= [];
  public Getspecs: any[] = [];
  public GetLaptops: any[] = [];
  // errorflag=false;

  doSomethingWithCurrentValue($event){
    //console.log(event)
  }

  constructor(@Inject(PLATFORM_ID) private platformId: Object,
              private route: ActivatedRoute,
              private httpService: HttpService,
              public sgflag: SimpleGlobal,
              private ref: ChangeDetectorRef,
              private http: Http,
              private router: Router) {




  }
  getAllCategories() {

    return this.http.get(this.url + 'search/GetAllCategories').map(response => response.json());

  }
  p: any;
  pageno: any;
  listviewfun(){
    this.listview=true;
    this.gridview=false;
  }
  categoryChanged(category){
    this.httpService.FilterProductsbyCategory(category,this.pageno,'full').subscribe(
      data => {
        console.log(data)
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );
  }
  gridviewfun(){
    this.listview=false;
    this.gridview=true;
  }
  pageChanged(event) {
    this.p = event;
    this.pageno = event;

    if (this.sort == "Best Match") {
      this.httpService.FilterProductsbyCategory(this.modelNo,this.pageno,'full').subscribe(
        data => {
          console.log(data)
          this.Getspecs = data;
          this.count = data['totalItems']

        }
      );

    }
    else if (this.sort == "Price: low to high") {
      this.httpService.FilterProductsbyCategoryASC(this.modelNo,this.pageno,'full').subscribe(
        data => {
          console.log(data);
          this.Getspecs = data;
          this.count = data['totalItems']

        }
      );


    }
    else if (this.sort == "Price: high to low") {
      this.httpService.FilterProductsbyCategoryDESC(this.modelNo,this.pageno,'full').subscribe(
        data => {
          console.log(data);
          this.Getspecs = data;
          this.count = data['totalItems']

        }
      );


    }


  }

  onClickSearch() {

    // //alertthis.searchedQuery)
    // if (this.modelNo == "mobile") {
    //   this.filterMobile();
    //
    // }
    // if (this.modelNo == "laptop") {
    //   this.filterLaptop();
    //
    // }
    // if (this.modelNo == "wearable") {
    //   this.filterWearable();
    //
    // }

  }

  oldcall: any;

  onClick() {
    this.oldcall = this.modelNo;
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        // Best Matchs to 0 if no query param provided.
        this.modelNo = params['Name'] || 0;
        // //alertthis.modelNo+" model")
        // //alertthis.modelNo)
      });
    //this.show="Search any "+this.modelNo;

    setTimeout(() => {
      this.pageno = 1;
      if (this.modelNo == "mobile") {
        this.getAllMobiles();
        // //alert"in mobile")

      }
      if (this.modelNo == "laptop") {
        this.getAllLaptop();
        //  //alert"in laptop")

      }
      if (this.modelNo == "wearable") {
        this.getAllWearable();
        //  //alert"in wear")

      }
      this.show = "Search any " + this.modelNo;

    }, 1000)


  }

  pagetogo = 1;

  onChangeOption(event) {
    this.sort = event.name;

    this.pageno = 1;

    if (this.sort == "Best Match") {
      this.pageno = 1;
      this.httpService.FilterProductsbyCategory(this.modelNo,this.pageno,'full').subscribe(
        data => {
          console.log(data)
          this.Getspecs = data;
          this.count = data['totalItems']

        }
      );

    }
    else if (this.sort == "Price: low to high") {
      // //alert"assda")
      this.pageno = 1;
      this.httpService.FilterProductsbyCategoryASC(this.modelNo,this.pageno,'full').subscribe(
        data => {
          console.log(data)
          this.Getspecs = data;
          this.count = data['totalItems']

        }
      );


    }
    else if (this.sort == "Price: high to low") {
      this.pageno = 1;
      this.httpService.FilterProductsbyCategoryDESC(this.modelNo,this.pageno,'full').subscribe(
        data => {
          console.log(data)
          this.Getspecs = data;
          this.count = data['totalItems']

        }
      );


    }
  }

  loadmore(event) {

    this.pagetogo = this.pagetogo + 1;

    this.ref.detectChanges();
    this.ref.reattach();

  }

  id: any;
  AllCount: any = [];
  flag = false;


  private url = environment.apiUrl;


  user: any;



  addInwishlist(item: any) {
    // alert("adding")
    this.httpService.Addwishlist(item).subscribe();
  }

  ngOnInit() {
    this.Categories =this.sgflag['Category']

    console.log(this.sgflag['Category'])
    this.httpService.getTrendsGame(1, 'full').subscribe(
      data => {


        this.Popular = data
      }
    );


    if (isPlatformBrowser(this.platformId)) {
      window.scrollTo(0, 0)
    }
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      //  //alert"call")
      this.modelNo = this.id;
      this.pageno = 1;
      this.sort = "Best Match"
      this.selectedOption = this.options[0]

      if (this.sort == "Best Match") {
        this.pageno = 1;
        this.httpService.FilterProductsbyCategory(this.modelNo,this.pageno,'full').subscribe(
          data => {
            console.log(data)
            this.Getspecs = data;
            this.count = data['totalItems']
            if(this.Getspecs['results'].length>0){
              this.apiflag=false;

            }
            else {
              this.apiflag=true;
            }
          },error2 => {
            if(this.Getspecs['results'].length>0){
              this.apiflag=false;

            }
            else {
              this.apiflag=true;
            }
          }
        );

      }
      else if (this.sort == "Price: low to high") {
        // //alert"assda")
        this.pageno = 1;
        this.httpService.FilterProductsbyCategoryASC(this.modelNo,this.pageno,'full').subscribe(
          data => {
            console.log(data)
            this.Getspecs = data;
            this.count = data['totalItems']
            if(this.Getspecs['results'].length>0){
              this.apiflag=false;

            }
            else {
              this.apiflag=true;
            }
          },
          error2 => {
            this.errorflag=true;
            console.log('error while calling data')
            if(this.Getspecs['results'].length>0){
              this.apiflag=false;

            }
            else {
              this.apiflag=true;
            }
          }
        );


      }
      else if (this.sort == "Price: high to low") {
        this.pageno = 1;
        this.httpService.FilterProductsbyCategoryDESC(this.modelNo,this.pageno,'full').subscribe(
          data => {
            console.log(data)
            this.Getspecs = data;
            this.count = data['totalItems']
            if(this.Getspecs['results'].length>0){
              this.apiflag=false;

            }
            else {
              this.apiflag=true;
            }
          },
          error2 => {
            this.errorflag=true;
            console.log('error while calling data')
            if(this.Getspecs['results'].length>0){
              this.apiflag=false;

            }
            else {
              this.apiflag=true;
            }
          }
        );


      }


      this.show = "Search any " + this.modelNo;
    });
    this.ref.detectChanges();
    this.ref.reattach();

    this.httpService.getpopular(1).subscribe(
      data => {
        const myArray = [];


        this.Popular = data
      }
    );

  }

  // filterMobile() {
  //   this.httpService.FilterMobile(this.searchedQuery, this.pageno, 'full').subscribe(
  //     data => {
  //       this.Getspecs = data;
  //       this.count = data['totalItems']
  //     }
  //   );
  //
  // }
  //
  // filterWearable() {
  //   this.httpService.FilterWearable(this.searchedQuery, this.pageno, 'full').subscribe(
  //     data => {
  //       this.Getspecs = data;
  //       this.count = data['totalItems']
  //
  //
  //     }
  //   );
  //
  // }
  //
  // filterLaptop() {
  //   this.httpService.FilterLaptop(this.searchedQuery, this.pageno, 'full').subscribe(
  //     data => {
  //       this.Getspecs = data;
  //       this.count = data['totalItems']
  //
  //
  //     }
  //   );
  //
  // }

  getAllMobiles() {

    this.httpService.getAllMobiles(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']


      }
    );

  }

  getAllLaptop() {

    // //alertthis.pageno)
    this.httpService.getAllLaptops(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }

  getTvs() {

    this.httpService.getAllTVS(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }

  getCams() {

    this.httpService.getAllCams(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }

  getSoftwares() {

    this.httpService.getAllSoftware(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }

  getSmarthomes() {

    this.httpService.getAllSmarthome(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }

  getGames() {

    this.httpService.getAllVideogames(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        console.log(data)
        this.count = data['totalItems']

      }
    );

  }

  getMovies() {

    this.httpService.getAllMovies(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }

  getAudio() {

    this.httpService.getAllAudio(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }

  getOfficeSupply() {

    this.httpService.getAllOffice(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }

  getApplinces() {

    this.httpService.getAllAppliences(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }

  getCarsElectronics() {

    this.httpService.getAllCarsElectronics(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }

  getToys() {

    this.httpService.getAllToys(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }

  getHealth() {

    this.httpService.getAllHealth(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }

  getAllWearable() {

    this.httpService.getWearables(this.pageno).subscribe(
      data => {
        this.Getspecs = data;
        this.count = data['totalItems']

      }
    );

  }
}
