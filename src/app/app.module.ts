import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import {routing, appRoutingProviders } from './app.routes';
import { PagerService } from './_services/index';
import { AppComponent } from './app.component';
import { HttpModule, XHRBackend, RequestOptions  } from '@angular/http';
import { PreloaderModule} from './components/preloader.module';
import { PreloaderService } from './services/preloader-service';
import { RecaptchaModule } from 'ng-recaptcha';
import { HttpService } from './services/http-service';
import { Ng2PaginationModule} from 'ng2-pagination';
import { DataService} from './shareduser.service';
import { PostService } from './services/post-service';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { Home1Component } from './home1/home1.component';
import { AllmobilesComponent } from './allmobiles/allmobiles.component';
import { MobilesComponent} from './mobiles/mobiles.component';
import { AuthGuard} from "./auth-gard/auth-gard.component";
import { OwlModule } from 'ngx-owl-carousel';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import { SimpleGlobal } from 'ng2-simple-global';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {RoundProgressModule} from 'angular-svg-round-progressbar';
import {Tagspipe} from "./mobiles/tagspipe";
import { ChartModule } from 'angular-highcharts';

export function httpServiceFactory(backend: XHRBackend, defaultOptions: RequestOptions, preloaderService: PreloaderService) {
  return new HttpService(backend, defaultOptions, preloaderService);
}
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ProductDetailsComponent,
    Home1Component,
    AllmobilesComponent,
    MobilesComponent,
    Tagspipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'universal-demo-v5' }),
    RoundProgressModule,
    HttpClientModule,
    BrowserTransferStateModule,
    FormsModule,
    ReactiveFormsModule,

    ChartModule,
    PreloaderModule,
    RecaptchaModule.forRoot(),
    routing,
    HttpModule,
    // CarouselModule,
    OwlModule,
    Ng2PaginationModule,
    MatButtonModule, MatCheckboxModule, MatSelectModule,MatCardModule,MatProgressSpinnerModule


  ],
  providers: [ appRoutingProviders, PagerService,AuthGuard,
    PreloaderService,
    DataService,
    SimpleGlobal,
    PostService,
    {
      provide: HttpService,

      useFactory: httpServiceFactory,
      deps: [XHRBackend, RequestOptions, PreloaderService]
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
