import { NgModule } from '@angular/core';
import {CommonModule } from '@angular/common'

import {PrivacypolicyComponent} from "../privacypolicy/privacypolicy.component"
import {Ng2PaginationModule} from 'ng2-pagination';
import { FormsModule } from '@angular/forms';
import {Detailrouting,appRoutingProviders} from './privacypolicy.routes';

import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import {PreloaderModule} from '../components/preloader.module'
@NgModule({
  declarations: [
    PrivacypolicyComponent,
    // PreloaderFull,
    // PreloaderSmall,

    //

  ],
  imports: [
    CommonModule,
    FormsModule,
    PreloaderModule,
    HttpModule,
    Ng2PaginationModule,
    Detailrouting
  ],

})
export class ResultModule { }
