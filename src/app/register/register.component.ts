import {Component, OnInit,Inject, PLATFORM_ID  } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Http} from '@angular/http';
import {isPlatformBrowser} from '@angular/common';

import {AlertService, UserService} from '../_services/index';
import swal from 'sweetalert';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserService, AlertService]
})
export class RegisterComponent implements OnInit {
  model: any = {};
  loading = false;
  Userloading = false;
  emailloading = false;
  UserError;
  emailError;

  passwrodmatch: boolean = true;

  constructor(private http: Http,
              @Inject(PLATFORM_ID) private platformId: Object,
              private router: Router,
              private route: ActivatedRoute,
              private userService: UserService,
              private alertService: AlertService) {
  }

  checkmail() {
    this.emailloading = true
    this.userService.check_email_unique(this.model.email)
      .subscribe(
        (response) => {
          // alert(this.model.email)

          this.emailloading = false;
          console.log(response)
          if (response.exists === 'Yes') {
            this.emailError = true;
          }
          if (response.exists !== 'Yes') {
            this.emailError = false;
          }

        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });

  }

  checkuser() {
    this.Userloading = true
    this.userService.verify_username(this.model.username.toLowerCase())
      .subscribe(
        (response) => {
          console.log(response.Res)
          this.Userloading = false;
          if (response.Res !== true) {
            this.UserError = false;
          }
          if (response.Res === true) {
            this.UserError = true;
          }

        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  sub:any;


  ngOnInit() {
    this.sub = this.route.queryParams.subscribe(params => {
      let verification = params['verification'];
      this.userService.verifyAccountEmail(verification).subscribe(data=>{
        console.log(data)


        if(data['code']==true){
          console.log('account verified now')
          this.router.navigate(['/login'])
        }
        else {
          swal("Something went wrong!", "", "warning");
        }
      })

    })
    this.model.policy = true
    this.model.newsLetter = true
    if (isPlatformBrowser(this.platformId)) {

      window.scrollTo(0, 0)
    }


  }

  check() {

    if (this.model.password == this.model.passwordRepeat) {
      this.passwrodmatch = true
    }
    else {
      this.passwrodmatch = false
    }

  }

  id: any;
  valid: any
  recaptcha=false;
  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response ${captchaResponse}:`);
    if(captchaResponse)
    {
      this.recaptcha=true;
    }
      // alert("hiii")
  }
  register() {


    if (this.emailError !== true && this.UserError !== true && this.recaptcha==true) {

this.valid=true;
      if (this.model.password === this.model.passwordRepeat) {
        if(this.model.newsLetter!=true)
        {
          this.model.newsLetter=false
        }
        this.model.username=this.model.username.toLowerCase()


        this.userService.create(this.model)
          .subscribe(
            data => {

              this.alertService.success('Registration successful', true);

              swal("Registeration Successful!", "Please check your email Inbox for Account Activation Instructions.", "success");
              // alert("Request Sucessfull")
              this.router.navigate(['/login']);

            },
            error => {
              this.alertService.error(error);
              this.loading = false;
            });
      }
      else {
        this.passwrodmatch = false
      }
    }
    else {
      this.valid = false
    }
  }

}
