import { NgModule } from '@angular/core';
import {CommonModule } from '@angular/common'

import {Registerrouting,appRoutingProviders} from './register.routes';
import {Ng2PaginationModule} from 'ng2-pagination';
import { FormsModule } from '@angular/forms';


import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import {RegisterComponent} from "../register/register.component"
import {PreloaderModule} from '../components/preloader.module'
import { RecaptchaModule } from 'ng-recaptcha';


@NgModule({
  declarations: [
    RegisterComponent,
    // PreloaderFull,
    // PreloaderSmall,

    //

  ],
  imports: [
    CommonModule,
    RecaptchaModule.forRoot(),
    FormsModule,
    PreloaderModule,
    HttpModule,
    Ng2PaginationModule,
    Registerrouting
  ],

})
export class ResultModule { }
