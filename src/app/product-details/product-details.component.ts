import {Component, OnInit, Inject, PLATFORM_ID} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {HttpService} from '../http.service';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Headers} from '@angular/http';
import {SimpleGlobal} from 'ng2-simple-global';

import {isPlatformBrowser} from '@angular/common';
import {AlertService, AuthenticationService} from '../_services/index';
import {review} from '../reviewModel/review'
import {environment} from '../../environments/environment';

import {Pipe, PipeTransform} from "@angular/core";

import { Chart } from 'angular-highcharts';
@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
  providers: [HttpService, AuthenticationService, AlertService]
})

@Pipe({name: 'round'})
export class ProductDetailsComponent implements OnInit {
  private urlapi = environment.apiUrl;



  reviewCount=10;
  errorflag:any;


  current=80;
  max=100;

  overallrating=50;
  overallStarrating=60/20;
  vendorSinglereviews: review;
  graph=false;
  options = [{'name': 'Best Match'}, {'name': 'Price: low to high'}, {'name': 'Price: high to low'}];
  selectedOption = this.options[0]
  sort: any;
  price:any;
  priceprcent:any;
  chartCategories=[]
  positiveseries=[]
  negativeseries=[]
  neutralseries=[]
  // chartCategories=['General','Price','Weight']

  chart : any;





  color = 'primary';
  colorwarn = 'warn';
  mode = 'determinate';
  value = 80;

  percentage= -25;


  creategraph(chartCategories:any){
    this.chart = new Chart({
      chart: {
        type: 'column'
      },
      credits: {
        enabled: false
      },
      title: {
        text: 'SHOPnROAR Feature Ranking AI (Based on Real Reviews)',
        style: {
          fontWeight: 'bold',
          fontSize:'25px'

        }
      },
      xAxis: {
        categories: chartCategories,
        labels: {
          style: {
            fontSize:'20px',
            textTransform: 'capitalize'
          }
        }
      },
      yAxis: {
        min:-100,
        max:125,
        title: {
          text: 'Reviews Ratings: Favorable/Negative Breakdown',
          style: {
            fontSize: '15px',
            textTransform: 'capitalize',
            color:'black'
          }
        },
        stackLabels: {
          enabled: true,
          style: {
            fontWeight: 'bold',
            fontSize:'18px'

          }
        }
      },
      tooltip: {
        headerFormat: '<b>{point.x} </b><br/>',
        pointFormat: '{series.name}: {point.y} %<br/>Total: {point.stackTotal} %'
      },
      legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,

        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
      },
      plotOptions: {
        column: {
          dataLabels: {
            enabled: true,
            format:'{point.y} %'

          }
        }
      },
        series: []
    });

  }
  getReviewsai(item){
    //console.log(item)
    this.http
      .get(this.urlapi+'products/filterReviewAI/'+item+'/')
      .subscribe(data => {
          this.ReviewsAI=data;
          console.log (this.ReviewsAI);
          //console.log (this.ReviewsAI.results[0].Reviews_AI.total_score)
          this.overallrating=Math.floor( this.ReviewsAI.results[0].Reviews_AI.total_score);
          this.overallStarrating=Math.ceil(this.overallrating/20);

          for (let aspects of this.ReviewsAI.results[0].Reviews_AI.results)
          {
            console.log(aspects.name);
            this.chartCategories.push(aspects.name)
            this.positiveseries.push(aspects.score[0])
            this.negativeseries.push(-aspects.score[1])
            this.neutralseries.push(aspects.score[2])
          }

          this.creategraph(this.chartCategories)

          this.chart.addSerie({
            name: 'Positive',
            data: this.positiveseries,
            color: '#53c722'

          });
          this.chart.addSerie({

            name: 'Negative',
            data: this.negativeseries,
            color: '#BB1228'
          });
          this.chart.addSerie({
            name: 'Neutral',
            data: this.neutralseries,
            color: '#20879D'

          });


          this.graph=true;
        },
        error => {
          this.http
            .get(this.urlapi+'products/filterReviewAI/'+'apple'+'/')
            .subscribe(data => {
              this.ReviewsAI=data;
              console.log (this.ReviewsAI);
              //console.log (this.ReviewsAI.results[0].Reviews_AI.total_score)
              this.overallrating=Math.floor( this.ReviewsAI.results[0].Reviews_AI.total_score)
              this.overallStarrating=Math.ceil(this.overallrating/20);

              for (let aspects of this.ReviewsAI.results[0].Reviews_AI.results)
              {
                console.log(aspects.name);
                this.chartCategories.push(aspects.name)
                this.positiveseries.push(aspects.score[0])
                this.negativeseries.push(-aspects.score[1])
                this.neutralseries.push(aspects.score[2])
              }

              this.creategraph(this.chartCategories)
              this.chart.addSerie({
                name: 'Positive',
                data: this.positiveseries,
                color: '#53c722'

              });
              this.chart.addSerie({

                name: 'Negative',
                data: this.negativeseries,
                color: '#BB1228'
              });
              this.chart.addSerie({
                name: 'Neutral',
                data: this.neutralseries,
                color: '#20879D'

              });

              this.graph=true;
            })
        }
      );
  }

  doSomethingWithCurrentValue($event){
    //console.log(event)
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  transform(value: number): number {
    return Math.round(value);
  }

  loadmorereviews(){
    this.reviewCount=this.reviewCount+10;
  }

  public loadScript() {
(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.12';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }

  onChangeOption(event) {
    if (event.name == 'Best Match') {
      this.naam = this.title.split(' ');
      this.i = 1;
      this.Q = this.naam[0]
      while (this.i < 6) {
        this.Q = this.Q + ' ' + this.naam[this.i];
        this.i = this.i + 1;
      }

      this.i = 0;
      // alert(this.Q +'best match')
      this.httpService.ProductsComparison(this.title, 'full').subscribe(
        data => {
          this.GetGroupspecs = data;
          this.Allproductsresults = (data['results'])
          this.Getspecs = data;

        }
      );

    }

    if (event.name == 'Price: low to high') {
      this.naam = this.title.split(' ');
      this.i = 1;
      this.Q = this.naam[0]
      while (this.i < 6) {
        this.Q = this.Q + ' ' + this.naam[this.i];
        this.i = this.i + 1;
      }

      this.i = 0;
      // alert(this.Q)
      this.httpService.searchGenericASC(this.Q, -1, -1, -1, -1, -1,1, 'full').subscribe(
        data => {
          this.GetGroupspecs = data;
          this.Allproductsresults = (data['results'])
          this.Getspecs = data;
          //////console.log(data)

          ////
          this.Allproductsresults.sort(function (a, b) {
            if (a.SNR_Price < b.SNR_Price) {
              return -1
            }
            else if (a.SNR_Price > b.SNR_Price) {
              return 1
            }
            else {
              return 0
            }
          })


          //////////
        }
      );

    }

    if (event.name == 'Price: high to low') {
      this.naam = this.title.split(' ');
      this.i = 1;
      this.Q = this.naam[0]
      while (this.i < 6) {
        this.Q = this.Q + ' ' + this.naam[this.i];
        this.i = this.i + 1;
      }

      this.i = 0;
      // alert(this.Q)
      this.httpService.searchGenericDESC(this.Q, -1, -1, -1, -1, -1,1, 'full').subscribe(
        data => {
          this.GetGroupspecs = data;
          this.Allproductsresults = (data['results'])
          this.Getspecs = data;
        }
      );

    }
  }

  getValue(event) {
    // //alert(event)
    this.star = event;
    // //alert(this.star)
  }

  msgflag = false;
  star = 0;

  constructor(@Inject(PLATFORM_ID) private platformId: Object,
              private httpService: HttpService,
              private authenticationService: AuthenticationService,
              private alertService: AlertService,
              private route: ActivatedRoute,
              public sgflag: SimpleGlobal,

              private http: HttpClient,
              public router: Router) {

    this.vendorSinglereviews = {
      review: 'none',
      vendorName: 'none'
    }
  }


  sub: any;
  modelNo: any;

  totalreview: any;
  Getspecs: any = [];
  GetsReviews: any = [];
  Reviewflag:any;
  Reviews: any = [];
  ReviewsAI: any = [];
  GetVendorReviews: any = [];


  GetGroupspecs: any = [];
  name: string;
  description: string;
  image="img";
  isCheck = false;
  image1="img";
  image2="img";
  GetLaptops: any = [];
  image3="img";
  pagetogo = 1;
  pagetogoproducts = 1;

  Popular: any = [];
  model: any = {};
  mail: string;
  naam: any;
  vendorReviews: any;
  Q: any;
  link: string;
  count = 1;
  temp: string;

  sendMail() {
    this.temp = this.title.replace(' ', '%20')
    this.temp = this.temp.replace(' ', '%20')
    this.temp = this.temp.replace(' ', '%20')
    this.temp = this.temp.replace(' ', '%20')

    //////console.log(this.temp)
    this.link = 'www.shopnroar.com/details?Model=' + this.modelNo + '&Name=' + this.temp
    //////console.log(this.link)
    this.mail = this.model.email
    // //alert(this.mail)
    this.httpService.sendLinkMail(this.mail, this.link).subscribe();

  }

  show() {
    this.isCheck = !this.isCheck;
    // //alert(this.isCheck)

  }

  brand: string;
  productID: string;
  productURL: string;
  upc: string;
  descr: string
  i = 0;

  search() {
    {

      this.naam = this.title.split(' ');
      this.i = 1;
      this.Q = this.naam[0]
      if(this.naam.length<5){
        while (this.i < this.naam.length) {
          this.Q = this.Q + ' ' + this.naam[this.i];
          this.i = this.i + 1;
        }
      }

      if(this.naam.length>5){
        while (this.i < this.naam.length/2) {
          this.Q = this.Q + ' ' + this.naam[this.i];
          this.i = this.i + 1;
        }
      }

      this.i = 0;
      // alert(this.Q)
      this.httpService.ProductsComparison(this.title, 'full').subscribe(
        data => {
          this.GetGroupspecs = data;
          this.Allproductsresults = (data['results'])
          this.Getspecs = data;

          try {
            this.image1 = this.Getspecs['results'][1]['SNR_ImageURL']
            this.image2 = this.Getspecs['results'][2]['SNR_ImageURL']
            this.image3 = this.Getspecs['results'][3]['SNR_ImageURL']

          } catch (e) {
            // this.image1 = this.Getspecs['results'][1]['SNR_ImageURL']

          }

          //console.log('image')
          //console.log(this.image3)

          // //////console.log(data)
          this.name = (this.Getspecs['results'][0]['SNR_Title'])
          this.productURL = (this.Getspecs['results'][0]['SNR_ProductURL'])

          this.description = this.Getspecs['results'][0]['SNR_Description']

          var re =/<(\w+)>|<\/(\w+)>/g;

          this.description=this.description.replace(re,'')
          // this.description=this.description.replace('</li>','')

          this.image = this.Getspecs['results'][0]['SNR_ImageURL']
          try {
            this.upc = this.Getspecs['results'][0]['SNR_UPC']
            this.brand = this.Getspecs['results'][0]['SNR_Brand']
            this.productID = this.Getspecs['results'][0]['SNR_SKU']
            while (this.productID != null) {
              this.productID = this.Getspecs['results'][this.i]['SNR_SKU']
              this.i = this.i + 1
            }
            this.i = 0;
            this.naam = this.productID.split(':');
            this.productID = (this.naam[1])
          }
          catch (e) {

          }


          if (!this.image2) {
            this.image1 = this.GetGroupspecs['results'][0]['SNR_ImageURL']
            this.image2 = this.GetGroupspecs['results'][1]['SNR_ImageURL']
            this.image3 = this.GetGroupspecs['results'][2]['SNR_ImageURL']

          }

// //////console.log(this.GetGroupspecs)
        }
      );
      // //alert(this.naam)
      // this.Q=this.title.substring(0,13)


      this.naam = this.title.split(' ');
      this.i = 1;

      if(this.naam.length<5){
        while (this.i < this.naam.length) {
          this.Q = this.Q + ' ' + this.naam[this.i];
          this.i = this.i + 1;
        }
      }

      if(this.naam.length>5){
        while (this.i < this.naam.length/2) {
          this.Q = this.Q + ' ' + this.naam[this.i];
          this.i = this.i + 1;
        }
      }

      this.i = 0;

//similar products
      this.httpService.ProductsComparison(this.title, 'full').subscribe(
        data => {
          this.Allresults = (data['results'])

        });



    }

  }

  flag = false;


  title: any;
  url: any;
  user: any;
  Allresults = []
  Allproductsresults = []
  Allproductsresultsgroupon = []

  // pagetogo = 1;

  loadmoreproducts() {
    this.pagetogoproducts = this.pagetogoproducts + 1;

    if (this.modelNo !== 'Visit site to see' && this.modelNo !== '00' && this.modelNo !== 'null') {

      this.naam = this.title.split(' ');
      this.i = 1;
      this.Q = this.naam[0]
      while (this.i < 6) {
        this.Q = this.Q + ' ' + this.naam[this.i];
        this.i = this.i + 1;
      }

      this.i = 0;
      this.httpService.ProductsComparison(this.title, 'full').subscribe(
        data => {

          this.GetGroupspecs = data;
          if (data['totalPages'] > this.pagetogoproducts) {
            this.msgflag = false;

            this.Allproductsresults = this.Allproductsresults.concat(data['results'])


          }

          else {
            this.msgflag = true
          }
// //////console.log(this.GetGroupspecs)
        }
      );


    }
    else {


      this.naam = this.title.split(' ');
      this.i = 1;
      this.Q = this.naam[0]
      while (this.i < 6) {
        this.Q = this.Q + ' ' + this.naam[this.i];
        this.i = this.i + 1;
      }

      this.i = 0;
      // //alert(this.Q)
      this.httpService.ProductsComparison(this.title,'full').subscribe(
        data => {

          this.Getspecs = data;
          if (data['totalPages'] > this.pagetogoproducts) {
            this.msgflag = false
            this.Allproductsresults = this.Allproductsresults.concat(data['results'])

            this.name = (this.Getspecs['results'][0]['SNR_Title'])
            this.description = this.Getspecs['results'][0]['SNR_Description']
            this.image = this.Getspecs['results'][0]['SNR_ImageURL']

            this.image1 = this.Getspecs['results'][1]['SNR_ImageURL']
            this.image2 = this.Getspecs['results'][2]['SNR_ImageURL']
            this.image3 = this.Getspecs['results'][3]['SNR_ImageURL']

          }
          else {
            this.msgflag = true
          }
        }
      );
    }

  }

  substr = 3;

  loadmore() {
    this.pagetogo = this.pagetogo + 1;

    if (this.substr > 0) {
      this.Q = this.Q.substring(0, 13 - this.substr)
      this.httpService.searchGeneric(this.Q, -1, -1, -1, -1,-1, this.pagetogo, 'full').subscribe(
        data => {
          this.Allresults = this.Allresults.concat(data['results'])

        });
      this.substr = this.substr - 3;
    }


  }

  titletoadd: string
  reviewtoadd: string
  submit = false
  totalReviews: any;

  addreview() {
    this.titletoadd = this.model.title
    this.reviewtoadd = this.model.review

    this.httpService.Add_Review(this.user, this.productID,this.star,this.titletoadd,  this.reviewtoadd).subscribe(data=>{
      //console.log(data)
    },error => {
      //console.log('error')

    })
    this.submit = true
    this.model.title = ''
    this.model.review = ''
    this.Reviews=[]
    var rev;
    this.naam = this.title.split(' ');
    this.i = 1;
    rev = this.naam[0]
    while (this.i < 3) {
      rev = rev + ' ' + this.naam[this.i];
      this.i = this.i + 1;
    }


    setTimeout(() => {

      this.getReviews(rev);
    },3000);
  }

  loading = true
  loginID: any
  status: any;

  login() {
    if (isPlatformBrowser(this.platformId)) {
      // //////console.log(this.model.username);
      // //////console.log(this.model.password);
      this.loading = true
      this.authenticationService.login(this.model.username, this.model.password)
        .subscribe(
          data => {
            this.sgflag['login']=true;

            // //alert('Loged In');
            // var currentUser = JSON.parse(localStorage.getItem('currentUser'));
            // var token = currentUser.token; // your token
            // // //alert(token)
            // this.router.navigate(['']);
            var currentUser = JSON.parse(localStorage.getItem('currentUser')) || 0;
            var token = currentUser.token; // your token
            this.user = currentUser.username


            this.flag = true

            this.authenticationService.loginID(this.model.username).subscribe(
              data => {
                this.loginID = data;
                this.status = true
                localStorage.setItem('userKey', JSON.stringify(data[0].id));

                // //////console.log(this.loginID[0].id)
              }
            )
          },
          error => {
            this.alertService.error(error);
            this.loading = false;
            this.status = false

          });

    }
  }

  verifylogin() {
    if (isPlatformBrowser(this.platformId)) {



    }
  }

  fbsharelink = 'https://www.facebook.com/sharer/sharer.php?u='

// link='http://www.shopnroar.com/';
  ngOnInit() {

    this.loadScript()

    if (isPlatformBrowser(this.platformId)) {

      window.scrollTo(0, 0)
    }
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.modelNo = params['Model'] || 0;
        this.title = params['Name'] || 0;
        this.price = params['Price'] || 0;
        this.priceprcent=this.price/4;
        // alert(this.priceprcent)
        this.url = params['URL'] || 0;
        this.fbsharelink = 'https://www.shopnroar.com/details?Model=' + this.modelNo + '&Name=' + this.title

        if(this.modelNo==0 && this.title==0 && this.url==0){
          this.errorflag=true;
        }

        try{
          this.title=this.title.replace('#',' ')
          this.title=this.title.replace('\'',' ')

        }
        catch (ex){


        }
        // this.link=this.link+this.modelNo+'/'+this.title+'/'+this.url


      });


    localStorage.setItem('Recommendation' , JSON.stringify(this.title ));

    this.link = 'www.shopnroar.com/details?Model=' + this.modelNo + '&Name=' + this.title


    var currentUser = JSON.parse(localStorage.getItem('currentUser')) || 0;
    if (currentUser !== 0) {
      this.user = currentUser.username
      // alert('already logged in..')
      this.flag = true

    }


    this.search()
    var rev;
    this.naam = this.title.split(' ');
    this.i = 1;
    rev = this.naam[0]
    while (this.i < 2) {
      rev = rev + ' ' + this.naam[this.i];
      this.i = this.i + 1;
    }

    this.getReviews(rev);
    this.getReviewsai(rev);
  }

  getReviews(item){
    //console.log(item)
    this.http
      .get(this.urlapi+'products/filterReview/'+item+'/')
      .subscribe(data => {
      this.Reviews=data
          this.Reviewflag=true;
      // console.log (this.Reviews)
      },
        error => {
          this.http
            .get(this.urlapi+'products/filterReview/apple').subscribe(data => {
              this.Reviews=data
              this.Reviewflag=true;
              // console.log (this.Reviews)
            })
        }
      );
  }
  Add_Review(user: string, pid: string, star: any, title: string,  review: string, ) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

  this.http.post(this.urlapi + 'review/add', {
        "SNR_Review_Title":title,
        "SNR_Review_Author" :user,
        "SNR_Review_Body" : review,
        "SNR_Review_Stars" :star,
        "Product" : pid
      },
      {}).subscribe(data => {
        //console.log ('data'+data)
      },
      error => {

      }
    );;


  }

}
