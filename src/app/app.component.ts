import {Component, OnInit, Inject, PLATFORM_ID} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TransferState, makeStateKey} from '@angular/platform-browser';
import {isPlatformBrowser} from '@angular/common';

import {HttpService} from './http.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [HttpService]
})
export class AppComponent implements OnInit {
  title = 'app';
  dogs: any;

  constructor(private http: HttpClient,
              private state: TransferState,
              private router: Router,
              @Inject(PLATFORM_ID) private platformId: Object) {
  }

  ngOnInit() {
    // this.dogs = this.state.get(DOGS_KEY, null as any);
    // this.http
    //   .get('https://dog.ceo/api/breeds/list/all')
    //   .subscribe(data => {
    //     console.log(data);
    //     this.dogs = data;
    //     this.state.set(DOGS_KEY, data as any);
    //   });
  }
  scrollTop(){
    if (isPlatformBrowser(this.platformId)) {
      window.scroll(0, 0);
    }
  }
}
