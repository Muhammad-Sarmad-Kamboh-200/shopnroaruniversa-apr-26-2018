import {CommonModule } from '@angular/common'

import { NgModule } from '@angular/core';
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import { HttpService } from '../services/http-service';
import { PreloaderService } from '../services/preloader-service';
import { PostService } from '../services/post-service';
import { PreloaderFull } from '../components/preloader-full/preloader-full';
import { PreloaderSmall } from '../components/preloader-small/preloader-small';
import {routing,appRoutingProviders} from '../app.routes';
import { PagerService } from '../_services/index';

export function httpServiceFactory(backend: XHRBackend, defaultOptions: RequestOptions, preloaderService: PreloaderService) {
  return new HttpService(backend, defaultOptions, preloaderService);
}


@NgModule({
  declarations: [
    PreloaderFull,
    PreloaderSmall,
  ],
  imports: [
    CommonModule
  ],
  providers: [ appRoutingProviders,PagerService,
    PreloaderService,
    PostService,
    {
      provide: HttpService,
      useFactory: httpServiceFactory,
      deps: [XHRBackend, RequestOptions, PreloaderService]
    }],
  exports:[
    PreloaderFull,
    PreloaderSmall,
  ]
})
export class PreloaderModule { }
