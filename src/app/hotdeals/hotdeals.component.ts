import {Component, OnInit} from '@angular/core';
import {HttpService} from '../http.service';

@Component({
  selector: 'app-hotdeals',
  templateUrl: './hotdeals.component.html',
  styleUrls: ['./hotdeals.component.scss'],
  providers: [HttpService]

})
export class HotdealsComponent implements OnInit {
  listview = false;
  gridview = true;
  count:any;
  public products: any[] = [];
  p:any;
  pageno:any;
  Popular: any = [];
  apiflag=false;


  constructor(private httpService: HttpService) {
  }

  listviewfun() {
    this.listview = true;
    this.gridview = false;
  }
  pageChanged(event) {
    // //alert"laptop")
    this.p = event;
    this.pageno = event;

    this.httpService.gethotdeals(this.pageno).subscribe(
      data => {
        this.products = data;
        this.count = data['totalItems'];
        console.log(this.products);

      }
    );

  }

    gridviewfun() {
    this.listview = false;
    this.gridview = true;
  }

  ngOnInit() {
    this.httpService.gethotdeals(this.pageno).subscribe(
      data => {
        this.products = data;
        this.count = data['totalItems'];
        console.log(this.products);
        if(this.products['results'].length>0){
          this.apiflag=false;

        }
        else {
          this.apiflag=true;
        }


      }
    );
    this.httpService.getTrendsGame(1, 'full').subscribe(
      data => {


        this.Popular = data
      }
    );
  }

}
