import {Component, OnInit} from '@angular/core';
import {HttpService} from '../http.service';
import {Http} from '@angular/http';
import {environment} from '../../environments/environment';
import {Router, ActivatedRoute, NavigationEnd} from '@angular/router';

@Component({
  selector: 'app-deals',
  templateUrl: './deals.component.html',
  styleUrls: ['./deals.component.scss'],
  providers: [HttpService]

})
export class DealsComponent implements OnInit {
  listview = false;
  gridview = true;
  private url = environment.apiUrl;
slice=20;
  errorflag=false;
  apiflag=false;

  sub:any;
  brand:any;

  count: any;
  public products: any[] = [];
  p: any;
  pageno: any;
  public Popular: any= [];
 public categories: any[] = [];
 public brands:any = [];


  constructor(private httpService: HttpService, private http: Http, private route: ActivatedRoute) {
  }

  listviewfun() {
    this.listview = true;
    this.gridview = false;
  }
  changebrand(brand){
    this.httpService.Filterdealsbybrand(brand,1,'full').subscribe(
      data => {
        this.products = data;
        this.count = data['totalItems'];
        console.log(this.products);

      }
    );


  }
  pageChanged(event) {
    // //alert"laptop")
    this.p = event;
    this.pageno = event;

  this.brandsearch()
  }

  gridviewfun() {
    this.listview = false;
    this.gridview = true;
  }
  loadmoreCategories(){
this.slice=this.slice+20;
  }
brandsearch(){
  // console.log(this.pageno)
    if(this.brand!=='all'){
      this.httpService.Filterdealsbybrand(this.brand,this.pageno,'full').subscribe(
        data => {
          this.products = data;
          this.count = data['totalItems'];
          console.log(this.products);
          if(this.products['results'].length>0){
            this.apiflag=false;

          }
          else {
            this.apiflag=true;
          }


        },
        error2 => {
          // alert('error')
          if(this.products['results'].length>0){
            this.apiflag=false;
          }
          else {
            this.apiflag=true;
          }
          console.log(this.apiflag)


        }
      );

    }
    else {
      this.httpService.getdailydeals(this.pageno).subscribe(
        data => {
          this.products = data;
          this.count = data['totalItems'];
          console.log(this.products);
          if(this.products['results'].length>0){
            this.apiflag=false;
          }
          else {
            this.apiflag=true;
          }
          console.log(this.apiflag)

        },
        error2 => {
          // alert('error')
          if(this.products['results'].length>0){
            this.apiflag=false;
          }
          else {
            this.apiflag=true;
          }
          console.log(this.apiflag)

        }
      );
    }


  }
  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.brand = params['id']; // (+) converts string 'id' to a number
      this.pageno=1;
      this.brandsearch()
    })

    setTimeout(() => {

      this.getdealsBrands().subscribe(
        data => {


          this.brands = data
        }
      );
    }, 1000)

    setTimeout(() => {

      this.getTrendsGame(1).subscribe(
        data => {


          this.Popular = data
        }
      );
    }, 1500)
  }

  getdealsCategories() {

    return this.http.get(this.url + '/products/dealsCategories').map(response => response.json());

  }

  getdealsBrands() {

    return this.http.get(this.url + '/products/dealsBrands').map(response => response.json());

  }

  getTrendsGame(page: any) {

    return this.http.get(this.url + 'trend/getTrendsgame?page=' + page).map(response => response.json());

  }

}
